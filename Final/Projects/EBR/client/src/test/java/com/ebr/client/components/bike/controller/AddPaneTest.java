package com.ebr.client.components.bike.controller;

import com.ebr.client.bean.bike.Bike;
import com.ebr.client.bean.bike.EBike;
import com.ebr.client.bean.bike.SingleBike;
import com.ebr.client.bean.bike.TwinBike;
import com.ebr.client.bean.station.Station;
import com.ebr.client.components.base.view.ListPane;
import com.ebr.client.components.bike.view.BikeAddDialog;
import com.ebr.client.serverapi.IServerApi;
import com.ebr.client.serverapi.ServerAPI;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class AddPaneTest {
    @Test
    public void testGetStation() {
        IServerApi serverApi = new ServerAPI();
        ArrayList<Station> stationList = serverApi.getStations(new HashMap<>());
        String[] stationIdList = null;

        if (stationList != null) {
            stationIdList = stationList.stream().map(station -> station.getId()).toArray(String[]::new);
        }

        assertEquals(stationList.size(), stationIdList.length);
    }

    @Test
    public void testAddSingleBike(){
        IServerApi serverApi = new ServerAPI();
        ArrayList<Bike> bikeArrayListBefore = serverApi.getBikes(new HashMap<>());
        SingleBike bike = new SingleBike("bike111", 10.5, "eco_bike11", new Date(), "BionX", 5.6, "sd", "bike20");
        Bike bikeAfterAdded = serverApi.addBike(bike);
        ArrayList<Bike> bikeArrayListAfter = serverApi.getBikes(new HashMap<>());

        assertEquals(bikeArrayListBefore.size(), bikeArrayListAfter.size()-1);
        assertEquals("bike111", bikeAfterAdded.getName());
        assertEquals(SingleBike.class, bikeAfterAdded.getClass());
    }

    @Test
    public void testAddTwinBike(){
        TwinBike bike = new TwinBike("bike111", 10.5, "eco_bike11", new Date(), "BionX", 5.6, "sd", "bike13");
        IServerApi serverApi = new ServerAPI();
        ArrayList<Bike> bikeArrayListBefore = serverApi.getBikes(new HashMap<>());
        Bike bikeAfterAdded = serverApi.addBike(bike);
        ArrayList<Bike> bikeArrayListAfter = serverApi.getBikes(new HashMap<>());

        assertEquals(bikeArrayListBefore.size(), bikeArrayListAfter.size()-1);
        assertEquals("bike111", bikeAfterAdded.getName());
        assertEquals(TwinBike.class, bikeAfterAdded.getClass());
    }

    @Test
    public void testAddEBike(){
        EBike bike = new EBike("bike111", 10.5, "eco_bike11", new Date(), "BionX", 5.6, "sd", "bike15", 10,10,10);
        IServerApi serverApi = new ServerAPI();
        ArrayList<Bike> bikeArrayListBefore = serverApi.getBikes(new HashMap<>());
        Bike bikeAfterAdded = serverApi.addBike(bike);
        ArrayList<Bike> bikeArrayListAfter = serverApi.getBikes(new HashMap<>());

        assertEquals(bikeArrayListBefore.size(), bikeArrayListAfter.size()-1);
        assertEquals("bike111", bikeAfterAdded.getName());
        assertEquals(EBike.class, bikeAfterAdded.getClass());
    }

}
