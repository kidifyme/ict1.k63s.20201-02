package com.ebr.client.serverapi.test;

import com.ebr.client.bean.creditcard.CreditCard;
import com.ebr.client.serverapi.ServerAPI;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class CardAPITest {
    private ServerAPI api = new ServerAPI();
    Map<String, String> queryParams = new HashMap<String, String>();

    @Test
    public void testGetCreditCards() {
        ArrayList<CreditCard> list = api.getCreditCards(null);
        System.out.println(list);
        assertEquals("Error in getCreditCards API!", list.size(), 3);
    }
}
