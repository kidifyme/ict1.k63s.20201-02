package com.ebr.client.serverapi.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;


@RunWith(Suite.class)
@Suite.SuiteClasses({SingleBikeAPITest.class})
public class BikeAPITest {

}