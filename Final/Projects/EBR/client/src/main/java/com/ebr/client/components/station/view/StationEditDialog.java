package com.ebr.client.components.station.view;

import com.ebr.client.bean.station.Station;
import com.ebr.client.components.base.event.eventargs.DataEditedEventArgs;
import com.ebr.client.components.base.view.EditDialog;

import javax.swing.*;

public class StationEditDialog extends EditDialog<Station> {
    private JTextField stationId;
    private JTextField stationName;
    private JTextField stationAddress;
    private JTextField numOfSingleBikes;
    private JTextField numOfTwinBikes;
    private JTextField numOfEBikes;
    private JTextField numOfEmptyDocks;

    public StationEditDialog(Station editBike) {
        super(editBike);
    }

    @Override
    protected void buildSaveButton() {
        JButton saveButton = new JButton("Save");

        saveButton.addActionListener(actionEvent -> {
            emitEvent(this, new DataEditedEventArgs<>(getNewData()));
            this.dispose();
        });

        c.gridx = 1;
        c.gridy = getLastRowIndex();
        getContentPane().add(saveButton, c);
    }

    @Override
    protected Station getNewData() {
        t.setStationName(stationName.getText());
        t.setStationAddress(stationAddress.getText());
        t.setNumOfSingleBikes(Integer.parseInt(numOfSingleBikes.getText()));
        t.setNumOfTwinBikes(Integer.parseInt(numOfTwinBikes.getText()));
        t.setNumOfEBikes(Integer.parseInt(numOfEBikes.getText()));
        t.setNumOfEmptyDocks(Integer.parseInt(numOfEmptyDocks.getText()));
        return t;
    }

    @Override
    protected void buildControls() {
        int row = getLastRowIndex();

        //Add station id field to panel (disable edit)
        JLabel stationIdLabel = new JLabel("ID: ");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(stationIdLabel, c);
        stationId = new JTextField(15);
        stationId.setText(t.getId());
        stationId.setEditable(false);
        c.gridx++;
        getContentPane().add(stationId, c);

        //Add station name field to panel
        row++;
        JLabel stationNameLabel = new JLabel("Name: ");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(stationNameLabel, c);
        stationName = new JTextField(15);
        stationName.setText(t.getStationName());
        c.gridx++;
        getContentPane().add(stationName, c);

        //Add station address field to panel
        row++;
        JLabel stationAddressLabel = new JLabel("Address: ");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(stationAddressLabel, c);
        stationAddress = new JTextField(15);
        stationAddress.setText(t.getStationAddress());
        c.gridx++;
        getContentPane().add(stationAddress, c);

        //Add no of single bikes field to panel (disable edit)
        row++;
        JLabel numOfSingleBikesLabel = new JLabel("Number of single bikes: ");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(numOfSingleBikesLabel, c);
        numOfSingleBikes = new JTextField(15);
        numOfSingleBikes.setText(String.valueOf(t.getNumOfSingleBikes()));
        numOfSingleBikes.setEditable(false);
        c.gridx++;
        getContentPane().add(numOfSingleBikes, c);

        //Add no of twin bikes field to panel (disable edit)
        row++;
        JLabel numOfTwinBikesLabel = new JLabel("Number of twin bikes: ");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(numOfTwinBikesLabel, c);
        numOfTwinBikes = new JTextField(15);
        numOfTwinBikes.setText(String.valueOf(t.getNumOfTwinBikes()));
        numOfTwinBikes.setEditable(false);
        c.gridx++;
        getContentPane().add(numOfTwinBikes, c);

        //Add no of electric bikes field to panel (disable edit)
        row++;
        JLabel numOfEBikesLabel = new JLabel("Number of electric bikes: ");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(numOfEBikesLabel, c);
        numOfEBikes = new JTextField(15);
        numOfEBikes.setText(String.valueOf(t.getNumOfEBikes()));
        numOfEBikes.setEditable(false);
        c.gridx++;
        getContentPane().add(numOfEBikes, c);

        //Add no of empty docks field to panel (disable edit?)
        row++;
        JLabel numOfEmptyDocksLabel = new JLabel("Number of empty docks: ");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(numOfEmptyDocksLabel, c);
        numOfEmptyDocks = new JTextField(15);
        numOfEmptyDocks.setText(String.valueOf(t.getNumOfEmptyDocks()));
        numOfEmptyDocks.setEditable(false);
        c.gridx++;
        getContentPane().add(numOfEmptyDocks, c);
    }


}
