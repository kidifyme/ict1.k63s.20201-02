package com.ebr.client.components.base.view;

import com.ebr.client.components.base.datasource.IDataSource;
import com.ebr.client.components.base.event.EventEmitterDelegate;
import com.ebr.client.components.base.event.IEventEmitter;
import com.ebr.client.components.base.event.eventargs.DataActionEventArgs;
import com.ebr.client.components.base.event.eventargs.DataSourceEventArgs;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * Displays a list of data items.
 *
 * @param <TData> a bean class type
 */
public abstract class ListPane<TData>
        extends EventSourceScrollPane<DataActionEventArgs> {

    /**
     * A container wrapping a {@link com.ebr.client.components.base.view.ItemPane}
     * and a data handling component.
     * <p/>
     * <b>ItemContainer</b> associates itself with
     * data item's index inside a data source.
     *
     * @param <TData> a bean class type
     */
    protected static abstract class ItemContainer<TData>
            extends JPanel {
        private int index;
        private ItemPane<TData> itemPane;

        protected IEventEmitter<DataActionEventArgs> eventEmitter;

        public ItemContainer() {
            setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
            setAlignmentX(0);
        }

        public int getIndex() {
            return index;
        }

        public void setIndex(int index) {
            this.index = index;
        }

        public void setEventEmitter(IEventEmitter<DataActionEventArgs> eventEmitter) {
            this.eventEmitter = eventEmitter;
        }

        public void setItemPane(ItemPane<TData> itemPane) {
            this.itemPane = itemPane;
        }

        public ItemPane<TData> getItemPane() {
            return itemPane;
        }

        protected abstract Component onCreateDataHandlingComponent();

        public void onLayoutControls() {
            removeAll();

            add(itemPane);
            add(onCreateDataHandlingComponent());
        }

        @Override
        public Dimension getMaximumSize() {
            return getPreferredSize();
        }
    }

    protected final IDataSource<TData> dataSource;
    protected final IEventEmitter<DataActionEventArgs> eventEmitter;

    protected final JPanel contentPane;

    public ListPane(IDataSource<TData> dataSource) {
        this.dataSource = dataSource;
        this.eventEmitter = new EventEmitterDelegate<>(this.listenerList);

        dataSource.addEventHandler((sender, eventArgs) -> onDataSourceEvent(eventArgs));

        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(10, 10, 0, 0));
        contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
        setViewportView(contentPane);

        setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        getVerticalScrollBar().setUnitIncrement(20);
        getHorizontalScrollBar().setUnitIncrement(20);
    }

    protected abstract ItemPane<TData> onCreateItemPane(TData data);

    protected abstract ItemContainer<TData> onCreateItemContainer();

    private void redrawAllItems() {
        contentPane.removeAll();

        // TODO: Reuse container
        for (int i = 0; i < dataSource.getSize(); i++) {
            contentPane.add(createItemContainer(i));
        }
    }

    private ItemContainer<TData> createItemContainer(int index) {
        ItemContainer<TData> container = onCreateItemContainer();

        TData data = dataSource.getItem(index);
        ItemPane<TData> itemPane = onCreateItemPane(data);
        itemPane.updateData(data);

        container.setIndex(index);
        container.setEventEmitter(this.eventEmitter);
        container.setItemPane(itemPane);

        container.onLayoutControls();

        return container;
    }

    protected void onDataSourceEvent(DataSourceEventArgs eventArgs) {
        switch (eventArgs.getType()) {
            case SourceChanged:
                onDataSourceChanged();
                break;
            case DataInserted:
                onDataInserted(eventArgs.getIndex());
                break;
            case DataDeleted:
                onDataDeleted(eventArgs.getIndex());
                break;
            case DataModified:
                onDataModified(eventArgs.getIndex());
                break;
        }
    }

    protected void onDataSourceChanged() {
        redrawAllItems();

        contentPane.revalidate();
        contentPane.repaint();
    }

    protected void onDataInserted(int index) {
        // Re-index items with index >= index
        for (int i = index; i < contentPane.getComponentCount(); i++) {
            @SuppressWarnings("unchecked")
            ItemContainer<TData> container = (ItemContainer<TData>) contentPane.getComponent(i);
            container.setIndex(i + 1);
        }

        // Insert a new item container
        ItemContainer<TData> itemContainer = createItemContainer(index);
        contentPane.add(itemContainer, index);

        contentPane.revalidate();
        contentPane.repaint();
    }

    protected void onDataDeleted(int index) {
        // Re-index items with index > index
        for (int i = index + 1; i < contentPane.getComponentCount(); i++) {
            @SuppressWarnings("unchecked")
            ItemContainer<TData> container = (ItemContainer<TData>) contentPane.getComponent(i);
            container.setIndex(i - 1);
        }

        // Remove item container
        contentPane.remove(index);

        contentPane.revalidate();
        contentPane.repaint();
    }

    protected void onDataModified(int index) {
        @SuppressWarnings("unchecked")
        ItemContainer<TData> container = (ItemContainer<TData>) contentPane.getComponent(index);
        container.getItemPane().updateData(dataSource.getItem(index));

        contentPane.revalidate();
        contentPane.repaint();
    }
}
