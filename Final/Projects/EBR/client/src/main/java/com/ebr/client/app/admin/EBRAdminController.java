package com.ebr.client.app.admin;

import com.ebr.client.components.bike.controller.AdminBikePageController;
import com.ebr.client.components.station.controller.AdminStationPageController;
import com.ebr.client.serverapi.IServerApi;
import com.ebr.client.serverapi.ServerAPI;

import javax.swing.*;
import java.awt.*;

public class EBRAdminController {
    private final JPanel rootPanel;

    private final IServerApi serverApi;

    public EBRAdminController() {
        rootPanel = new JPanel();

        BorderLayout layout = new BorderLayout();
        rootPanel.setLayout(layout);

        // Server api
        serverApi = new ServerAPI();

        // Station page controller
        AdminStationPageController adminStationPageController = new AdminStationPageController(serverApi);
        adminStationPageController.fireSearchEvent();

        // Bike page controller
        AdminBikePageController adminBikePageController = new AdminBikePageController(serverApi);
        adminBikePageController.fireSearchEvent();

        JTabbedPane tabbedPane = new JTabbedPane();
        tabbedPane.addTab("Stations", null, adminStationPageController.getPagePane());
        tabbedPane.addTab("Bikes", null, adminBikePageController.getPagePane());

        rootPanel.add(tabbedPane, BorderLayout.CENTER);
    }

    public JPanel getRootPanel() {
        return rootPanel;
    }
}
