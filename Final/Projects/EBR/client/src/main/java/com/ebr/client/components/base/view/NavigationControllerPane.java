package com.ebr.client.components.base.view;

import com.ebr.client.components.base.event.eventargs.NavigationControllerEventArgs;

import javax.swing.*;
import java.awt.*;

public final class NavigationControllerPane extends EventSourcePanel<NavigationControllerEventArgs> {
    private final JButton navigateBackwardButton;
    private final JButton navigateForwardButton;

    public NavigationControllerPane() {
        BoxLayout layout = new BoxLayout(this, BoxLayout.X_AXIS);
        setLayout(layout);

        // Backward button
        navigateBackwardButton = new JButton("<");
        navigateBackwardButton.addActionListener(e -> emitEvent(this, new NavigationControllerEventArgs(NavigationControllerEventArgs.NavigationType.Backward)));
        add(navigateBackwardButton);

        // Forward button
        navigateForwardButton = new JButton(">");
        navigateForwardButton.addActionListener(e -> emitEvent(this, new NavigationControllerEventArgs(NavigationControllerEventArgs.NavigationType.Forward)));
        add(navigateForwardButton);
    }

    public void setNavigateBackwardEnabled(boolean enabled) {
        navigateBackwardButton.setEnabled(enabled);
    }

    public void setNavigateForwardEnabled(boolean enabled) {
        navigateForwardButton.setEnabled(enabled);
    }

    @Override
    public Dimension getMaximumSize() {
        return getPreferredSize();
    }
}
