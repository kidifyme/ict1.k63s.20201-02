package com.ebr.client.components.base.event;

import com.ebr.client.components.base.event.eventargs.EventArgs;

/**
 * An event source lets listeners/subscribers register <b>event handlers</b>.
 * Event listeners are notified when an event occurs.
 * <p/>
 * Classes that can emit events
 * implement this interface, and any class interested in
 * processing those events can do so by
 * implementing {@link com.ebr.client.components.base.event.IEventHandler}.
 *
 * @param <TEventArgs> a custom class that provides event information
 */
public interface IEventSource<TEventArgs extends EventArgs> {
    void addEventHandler(IEventHandler<TEventArgs> eventHandler);

    void removeEventHandler(IEventHandler<TEventArgs> eventHandler);
}
