package com.ebr.client.bean.station;

import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * Bean class representing a bike station.
 */
@JsonTypeName("station")
public class Station {
    private String id;
    private String stationName;
    private String stationAddress;
    private int numOfSingleBikes;
    private int numOfTwinBikes;
    private int numOfEBikes;
    private int numOfEmptyDocks;

    @Override
    public String toString() {
        return "Station{" +
                "id='" + id + '\'' +
                ", stationName='" + stationName + '\'' +
                ", stationAddress='" + stationAddress + '\'' +
                ", numOfSingleBikes=" + numOfSingleBikes +
                ", numOfTwinBikes=" + numOfTwinBikes +
                ", numOfEBikes=" + numOfEBikes +
                ", numOfEmptyDocks=" + numOfEmptyDocks +
                '}';
    }

    public Station() {
        super();
    }

    public Station(String id, String stationName, String stationAddress, int numOfSingleBikes, int numOfTwinBikes, int numOfEBikes, int numOfEmptyDocks) {
        this.id = id;
        this.stationName = stationName;
        this.stationAddress = stationAddress;
        this.numOfSingleBikes = numOfSingleBikes;
        this.numOfTwinBikes = numOfTwinBikes;
        this.numOfEBikes = numOfEBikes;
        this.numOfEmptyDocks = numOfEmptyDocks;
    }

    public String getId() {
        return id;
    }

    public Station(String id, String stationName, String stationAddress) {
        this.id = id;
        this.stationName = stationName;
        this.stationAddress = stationAddress;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getStationAddress() {
        return stationAddress;
    }

    public void setStationAddress(String stationAddress) {
        this.stationAddress = stationAddress;
    }

    public int getNumOfSingleBikes() {
        return numOfSingleBikes;
    }

    public void setNumOfSingleBikes(int numOfSingleBikes) {
        this.numOfSingleBikes = numOfSingleBikes;
    }

    public int getNumOfTwinBikes() {
        return numOfTwinBikes;
    }

    public void setNumOfTwinBikes(int numOfTwinBikes) {
        this.numOfTwinBikes = numOfTwinBikes;
    }

    public int getNumOfEBikes() {
        return numOfEBikes;
    }

    public void setNumOfEBikes(int numOfEBikes) {
        this.numOfEBikes = numOfEBikes;
    }

    public int getNumOfEmptyDocks() {
        return numOfEmptyDocks;
    }

    public void setNumOfEmptyDocks(int numOfEmptyDocks) {
        this.numOfEmptyDocks = numOfEmptyDocks;
    }

    public boolean match(Station station) {
        if (station == null)
            return true;


        if (station.id != null && !station.id.equals("") && !this.id.contains(station.id)) {
            return false;
        }
        if (station.stationName != null && !station.stationName.equals("") && !this.stationName.contains(station.stationName)) {
            return false;
        }
        if (station.stationAddress != null && !station.stationAddress.equals("") && !this.stationAddress.contains(station.stationAddress)) {
            return false;
        }
        return true;
    }
}
