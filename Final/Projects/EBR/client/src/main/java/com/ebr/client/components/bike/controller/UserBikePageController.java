package com.ebr.client.components.bike.controller;

import com.ebr.client.app.user.UserInfo;
import com.ebr.client.bean.bike.Bike;
import com.ebr.client.components.base.controller.PageController;
import com.ebr.client.components.base.datasource.IDataSource;
import com.ebr.client.components.base.event.IEventHandler;
import com.ebr.client.components.base.event.eventargs.DataActionEventArgs;
import com.ebr.client.components.base.event.eventargs.EventArgs;
import com.ebr.client.components.base.view.ListPane;
import com.ebr.client.components.base.view.PagePane;
import com.ebr.client.components.base.view.SearchPane;
import com.ebr.client.components.bike.datasource.BikeDataSource;
import com.ebr.client.components.bike.event.eventargs.RentActionEventArgs;
import com.ebr.client.components.bike.view.*;
import com.ebr.client.serverapi.IServerApi;
import com.ebr.client.serverapi.model.PostRentalRentResponse;
import com.ebr.client.serverapi.model.PostRentalReturnResponse;
import com.ebr.client.serverapi.model.RentalRentRequest;
import com.ebr.client.serverapi.model.RentalReturnRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class UserBikePageController extends PageController<Bike> {
    private final IServerApi serverApi;
    private BikeDataSource bikeDataSource;
    private String containingStationId;

    private BikeRentingPane rentingPane;

    private final IEventHandler<EventArgs> rentActionProcessedEventHandler;

    public UserBikePageController(IServerApi serverApi, IEventHandler<EventArgs> rentActionProcessedEventHandler) {
        super();
        this.serverApi = serverApi;
        this.rentActionProcessedEventHandler = rentActionProcessedEventHandler;
    }

    public void setContainingStationId(String stationId) {
        this.containingStationId = stationId;
        ((UserBikeSearchPane) searchPane).setContainingStationId(stationId);
    }

    public void setRentalStatus(boolean renting) {
        rentingPane.setRentalStatus(renting);
    }

    public void clearChosenBikeCode() {
        rentingPane.clearBikeCode();
    }

    @Override
    protected void onLinkingComponents() {
        IDataSource<Bike> dataSource = onCreateDataSource();

        // Search pane
        searchPane = onCreateSearchPane();
        searchPane.addEventHandler((sender, eventArgs) -> onSearchEvent(eventArgs.getSearchParams()));

        // List pane
        listPane = onCreateListPane(dataSource);
        listPane.addEventHandler((sender, eventArgs) -> onDataActionEvent(eventArgs));

        // Renting pane
        rentingPane = onCreateBikeRentingPane();
        rentingPane.addEventHandler((sender, eventArgs) -> onRentActionEvent(eventArgs));

        // Page pane
        pagePane = onCreatePagePane();
        pagePane.buildControls();
    }

    private BikeRentingPane onCreateBikeRentingPane() {
        return new BikeRentingPane();
    }

    @Override
    protected IDataSource<Bike> onCreateDataSource() {
        bikeDataSource = new BikeDataSource();
        return bikeDataSource;
    }

    @Override
    protected SearchPane onCreateSearchPane() {
        return new UserBikeSearchPane();
    }

    @Override
    protected ListPane<Bike> onCreateListPane(IDataSource<Bike> dataSource) {
        return new UserBikeListPane(dataSource);
    }

    @Override
    protected PagePane<Bike> onCreatePagePane() {
        return new UserBikePagePane(searchPane, listPane, rentingPane);
    }

    @Override
    protected void onDataActionEvent(DataActionEventArgs eventArgs) {
        String bikeCode = bikeDataSource.getItem(eventArgs.getIndex()).getCode();
        rentingPane.setBikeCode(bikeCode);
    }

    @Override
    protected void onSearchEvent(Map<String, String> searchParams) {
        ArrayList<Bike> result = serverApi.getBikes(searchParams);
        bikeDataSource.setSource(result);
    }

    private void rentButtonPressed(RentActionEventArgs rentActionEventArgs) {
        // Get preview rental request
        RentalRentRequest rentalRentRequest = new RentalRentRequest(true, rentActionEventArgs.getBikeCode(), UserInfo.getCreditCardNumber());
        PostRentalRentResponse postRentalRentResponse = serverApi.addRental(rentalRentRequest);

        // Validate response
        if (postRentalRentResponse != null) {
            // Prepare "bike code -> bike" query request
            HashMap<String, String> queryParams = new HashMap<>();
            queryParams.put("code", rentActionEventArgs.getBikeCode());
            queryParams.put("kind", "singlebike,twinbike,ebike");

            // Get bike
            Bike bike = serverApi.getBikes(queryParams).get(0);

            // Show rent dialog
            BikeRentDialog bikeRentDialog = new BikeRentDialog(bike, postRentalRentResponse);
            bikeRentDialog.addEventHandler((sender, eventArgs) -> {
                RentalRentRequest request = new RentalRentRequest(false, rentActionEventArgs.getBikeCode(), UserInfo.getCreditCardNumber());
                PostRentalRentResponse response = serverApi.addRental(request);

                if (response != null && response.getStatus().equals("ok")) {
                    onRentActionEventProcessed();
                }
            });
            bikeRentDialog.setVisible(true);
        }
    }

    private void returnButtonPressed(RentActionEventArgs rentActionEventArgs) {
        // Get preview rental request
        RentalReturnRequest rentalReturnRequest = new RentalReturnRequest(true, containingStationId, UserInfo.getCreditCardNumber());
        PostRentalReturnResponse postRentalReturnResponse = serverApi.returnBike(rentalReturnRequest);

        // Validate response
        if (postRentalReturnResponse != null) {
            // Show return dialog
            BikeReturnDialog bikeReturnDialog = new BikeReturnDialog(postRentalReturnResponse);
            bikeReturnDialog.addEventHandler((sender, eventArgs) -> {
                RentalReturnRequest request = new RentalReturnRequest(false, containingStationId, UserInfo.getCreditCardNumber());
                PostRentalReturnResponse response = serverApi.returnBike(request);

                if (response != null && response.getStatus().equals("ok")) {
                    onRentActionEventProcessed();
                }
            });
            bikeReturnDialog.setVisible(true);
        }
    }

    protected void onRentActionEventProcessed() {
        if (rentActionProcessedEventHandler != null) {
            rentActionProcessedEventHandler.onEvent(this, new EventArgs());
        }

        rentingPane.clearBikeCode();
    }

    protected void onRentActionEvent(RentActionEventArgs rentActionEventArgs) {
        switch (rentActionEventArgs.getRentAction()) {
            case Rent:
                rentButtonPressed(rentActionEventArgs);
                break;
            case Return:
                returnButtonPressed(rentActionEventArgs);
                break;
        }
    }
}
