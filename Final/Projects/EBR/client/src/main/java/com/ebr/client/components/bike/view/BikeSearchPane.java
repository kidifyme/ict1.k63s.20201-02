package com.ebr.client.components.bike.view;

import com.ebr.client.components.base.view.SearchPane;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Map;

public class BikeSearchPane extends SearchPane {
    protected JTextField stationID;
    protected JTextField nameField;
    protected JTextField licenseField;
    protected JTextField manufacturerField;
    protected JTextField codeField;
    protected JCheckBox typeField1;
    protected JCheckBox typeField2;
    protected JCheckBox typeField3;

    @Override
    public void buildControls() {
        //Station ID input field
        JLabel stationIDLabel = new JLabel("Station ID: ");
        stationID = new JTextField();

        //Name input field
        JLabel nameLabel = new JLabel("Name: ");
        nameField = new JTextField();

        //Bike license input field
        JLabel licenseLabel = new JLabel("License: ");
        licenseField = new JTextField();

        //Manufacturer input field
        JLabel manufacturerLabel = new JLabel("Manufacturer: ");
        manufacturerField = new JTextField();

        //Code input field
        JLabel codeLabel = new JLabel("Bike code: ");
        codeField = new JTextField();

        //Checkbox for bike type
        JLabel typeLabel = new JLabel("Type: ");
        JPanel typePanel = new JPanel();
        typeField1 = new JCheckBox("Single Bike");
        typeField1.setSelected(true);
        typeField2 = new JCheckBox("Twin Bike");
        typeField2.setSelected(true);
        typeField3 = new JCheckBox("Electrical Bike");
        typeField3.setSelected(true);

        typePanel.add(typeField1);
        typePanel.add(typeField2);
        typePanel.add(typeField3);

        //Add name to panel
        addComponentsToPanel(stationIDLabel, stationID);

        //Add name to panel
        addComponentsToPanel(nameLabel, nameField);

        //Add bike license to panel
        addComponentsToPanel(licenseLabel, licenseField);

        //Add manufacturer to panel
        addComponentsToPanel(manufacturerLabel, manufacturerField);

        //Add code to panel
        addComponentsToPanel(codeLabel, codeField);

        //Add bike type to panel
        //addComponentsToPanel(typeLabel,typeField1,typeField2,typeField3);
        addComponentsToPanel(typeLabel, typePanel);
    }

    protected void addComponentsToPanel(JLabel label, JCheckBox box) {
        gbc.gridx = 0;
        gbc.gridy++;
        add(label, gbc);

        gbc.gridx++;
        add(box, gbc);
    }

    protected void addComponentsToPanel(JLabel label, JTextField field) {
        gbc.gridx = 0;
        gbc.gridy++;
        add(label, gbc);

        gbc.gridx++;
        add(field, gbc);
    }

    protected void addComponentsToPanel(JLabel label, JPanel panel) {
        gbc.gridx = 0;
        gbc.gridy++;
        add(label, gbc);

        gbc.gridx++;
        add(panel, gbc);
    }

    @Override
    public Map<String, String> getQueryParams() {
        Map<String, String> params = super.getQueryParams();

        if (!stationID.getText().trim().equals("")) {
            params.put("stationId", stationID.getText().trim());
        }

        if (!nameField.getText().trim().equals("")) {
            params.put("name", nameField.getText().trim());
        }

        if (!licenseField.getText().trim().equals("")) {
            params.put("licensePlate", licenseField.getText().trim());
        }

        if (!manufacturerField.getText().trim().equals("")) {
            params.put("manufacturer", manufacturerField.getText().trim());
        }

        if (!codeField.getText().trim().equals("")) {
            params.put("code", codeField.getText().trim());
        }

        ArrayList<String> kinds = new ArrayList<>(3);

        if (typeField1.isSelected()) {
            kinds.add("singlebike");
        }

        if (typeField2.isSelected()) {
            kinds.add("twinbike");
        }

        if (typeField3.isSelected()) {
            kinds.add("ebike");
        }
        params.put("kind", String.join(",", kinds));
        return params;
    }
}
