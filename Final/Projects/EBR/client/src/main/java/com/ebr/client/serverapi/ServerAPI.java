package com.ebr.client.serverapi;

import com.ebr.client.bean.bike.Bike;
import com.ebr.client.bean.creditcard.CreditCard;
import com.ebr.client.bean.station.Station;
import com.ebr.client.serverapi.model.*;

import javax.ws.rs.client.*;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Map;

public class ServerAPI implements IServerApi {
    public static final String PATH = "http://localhost:8080/";

    private Client client;

    public ServerAPI() {
        client = ClientBuilder.newClient();
    }

    @Override
    public ArrayList<Bike> getBikes(Map<String, String> queryParams) {
        WebTarget webTarget = client.target(PATH).path("bikes");


        if (queryParams != null) {
            for (String key : queryParams.keySet()) {
                String value = queryParams.get(key);
                webTarget = webTarget.queryParam(key, value);
            }
        }


        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();
        ArrayList<Bike> res = response.readEntity(new GenericType<ArrayList<Bike>>() {
        });
        return res;
    }

    @Override
    public Bike updateBike(Bike bike) {
        WebTarget webTarget = client.target(PATH).path("bikes").path(bike.getCode());

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.put(Entity.entity(bike, MediaType.APPLICATION_JSON));
        return response.readEntity(Bike.class);
    }

    @Override
    public Bike addBike(Bike bike) {
        WebTarget webTarget = client.target(PATH).path("bikes");

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.post(Entity.entity(bike, MediaType.APPLICATION_JSON));
        return response.readEntity(Bike.class);
    }

    @Override
    public Response deleteBike(Bike bike) {
        WebTarget webTarget = client.target(PATH).path("bikes").path(bike.getCode());

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        return invocationBuilder.delete();
    }

    @Override
    public ArrayList<CreditCard> getCreditCards(Map<String, String> queryParams) {
        WebTarget webTarget = client.target(PATH).path("cards");


        if (queryParams != null) {
            for (String key : queryParams.keySet()) {
                String value = queryParams.get(key);
                webTarget = webTarget.queryParam(key, value);
            }
        }

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();
        ArrayList<CreditCard> res = response.readEntity(new GenericType<ArrayList<CreditCard>>() {
        });
        return res;
    }

    @Override
    public ArrayList<Station> getStations(Map<String, String> queryParams) {
        WebTarget webTarget = client.target(PATH).path("stations");

        if (queryParams != null) {
            for (String key : queryParams.keySet()) {
                String value = queryParams.get(key);
                webTarget = webTarget.queryParam(key, value);
            }
        }

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();
        ArrayList<Station> res = response.readEntity(new GenericType<ArrayList<Station>>() {
        });
        return res;
    }

    @Override
    public Station updateStation(Station station) {
        WebTarget webTarget = client.target(PATH).path("stations").path(station.getId());

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.put(Entity.entity(station, MediaType.APPLICATION_JSON));
        return response.readEntity(Station.class);
    }

    @Override
    public Station addStation(Station station) {
        WebTarget webTarget = client.target(PATH).path("stations");

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.post(Entity.entity(station, MediaType.APPLICATION_JSON));
        return response.readEntity(Station.class);
    }

    @Override
    public Response deleteStation(Station station) {
        WebTarget webTarget = client.target(PATH).path("stations").path(station.getId());

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        return invocationBuilder.delete();
    }

    @Override
    public GetRentalResponse getRental(Map<String, String> queryParams) {
        WebTarget webTarget = client.target(PATH).path("rental");


        if (queryParams != null) {
            for (String key : queryParams.keySet()) {
                String value = queryParams.get(key);
                webTarget = webTarget.queryParam(key, value);
            }
        }

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();
        GetRentalResponse res = response.readEntity(new GenericType<GetRentalResponse>() {
        });
        return res;
    }

    @Override
    public PostRentalRentResponse addRental(RentalRentRequest rentalRentRequest) {
        WebTarget webTarget = client.target(PATH).path("rental-rent");

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.post(Entity.entity(rentalRentRequest, MediaType.APPLICATION_JSON));
        return response.readEntity(PostRentalRentResponse.class);
    }

    @Override
    public PostRentalReturnResponse returnBike(RentalReturnRequest rentalReturnRequest) {
        WebTarget webTarget = client.target(PATH).path("rental-return");

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.post(Entity.entity(rentalReturnRequest, MediaType.APPLICATION_JSON));
        return response.readEntity(PostRentalReturnResponse.class);
    }
}
