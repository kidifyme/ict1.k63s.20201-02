package com.ebr.client.components.status.view;

import com.ebr.client.components.base.event.eventargs.EventArgs;
import com.ebr.client.components.base.view.EventSourcePanel;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public final class StatusPane extends EventSourcePanel<EventArgs> {
    private GridBagLayout layout;
    private GridBagConstraints reusableConstraints;

    private JLabel rentedBikeInfoValueLabel;
    private JLabel rentingTimeValueLabel;

    public StatusPane() {
        buildControls();
    }

    private void buildControls() {
        layout = new GridBagLayout();
        setLayout(layout);
        reusableConstraints = new GridBagConstraints();
        reusableConstraints.fill = GridBagConstraints.HORIZONTAL;
        reusableConstraints.weightx = 1;

        int row = 0;

        // Label bike info (text)
        reusableConstraints.gridx = 0;
        reusableConstraints.gridy = row;
        JLabel rentedBikeInfoTextLabel = new JLabel("Rented bike: ");
        add(rentedBikeInfoTextLabel, reusableConstraints);

        // Label bike info (value)
        reusableConstraints.gridx = 1;
        reusableConstraints.gridy = row;
        rentedBikeInfoValueLabel = new JLabel("None");
        add(rentedBikeInfoValueLabel, reusableConstraints);

        row++;
        // Label bike info (text)
        reusableConstraints.gridx = 0;
        reusableConstraints.gridy = row;
        JLabel rentingTimeTextLabel = new JLabel("Rented time: ");
        add(rentingTimeTextLabel, reusableConstraints);

        // Label bike info (value)
        reusableConstraints.gridx = 1;
        reusableConstraints.gridy = row;
        rentingTimeValueLabel = new JLabel("N/A");
        add(rentingTimeValueLabel, reusableConstraints);

        // Update button container
        JPanel buttonContainer = new JPanel();
        buttonContainer.setBorder(new EmptyBorder(0, 5, 0, 0));

        // Update button
        JButton updateButton = new JButton("Update");
        updateButton.addActionListener(e -> fireRequestUpdateEvent());
        buttonContainer.add(updateButton);

        reusableConstraints.gridx = 2;
        reusableConstraints.gridy = 0;
        add(buttonContainer, reusableConstraints);
    }

    public void fireRequestUpdateEvent() {
        emitEvent(this, new EventArgs());
    }

    public void updateStatus(String bikeType, String bikeCode, int rentingTime) {
        rentedBikeInfoValueLabel.setText(String.format("%s - %s", bikeType, bikeCode));
        rentingTimeValueLabel.setText(String.format("%d (min)", rentingTime));
    }

    public void updateStatus() {
        rentedBikeInfoValueLabel.setText("None");
        rentingTimeValueLabel.setText("N/A");
    }

    @Override
    public Dimension getMaximumSize() {
        return getPreferredSize();
    }
}
