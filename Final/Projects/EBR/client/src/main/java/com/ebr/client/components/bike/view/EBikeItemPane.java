package com.ebr.client.components.bike.view;

import com.ebr.client.bean.bike.EBike;

import javax.swing.*;

public class EBikeItemPane extends BikeItemPane {
    private JLabel batteryPercentage;
    private JLabel loadCycles;
    private JLabel timeRemaining;

    public EBikeItemPane() {
        super();
    }

    @Override
    protected void onBuildControls() {
        super.onBuildControls();

        int row = getLastRowIndex();

        //Battery Percentage
        row++;
        reusableConstraints.gridx = 0;
        reusableConstraints.gridy = row;
        batteryPercentage = new JLabel();
        add(batteryPercentage, reusableConstraints);

        //Load Cycles
        row++;
        reusableConstraints.gridx = 0;
        reusableConstraints.gridy = row;
        loadCycles = new JLabel();
        add(loadCycles, reusableConstraints);

        //Time Remaining
        row++;
        reusableConstraints.gridx = 0;
        reusableConstraints.gridy = row;
        timeRemaining = new JLabel();
        add(timeRemaining, reusableConstraints);
    }

    @Override
    protected void displayData() {
        super.displayData();
        EBike eBikeData = (EBike) data;
        batteryPercentage.setText(String.format("Battery percentage: %s", eBikeData.getBatteryPercentage()));
        loadCycles.setText(String.format("Load cycles: %s", eBikeData.getLoadCycles()));
        timeRemaining.setText(String.format("Time remaining: %s", eBikeData.getTimeRemaining()));
    }
}
