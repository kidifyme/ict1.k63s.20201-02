package com.ebr.client.components.base.event.eventargs;

import java.util.Map;

public class SearchEventArgs extends EventArgs {
    private Map<String, String> searchParams;

    public SearchEventArgs(Map<String, String> searchParams) {
        this.searchParams = searchParams;
    }

    public Map<String, String> getSearchParams() {
        return searchParams;
    }

    public void setSearchParams(Map<String, String> searchParams) {
        this.searchParams = searchParams;
    }
}
