package com.ebr.client.serverapi.model;

public class PostRentalRentResponse {
    private String status;
    private double balance;
    private double depositAmount;
    private int rentalId;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double getDepositAmount() {
        return depositAmount;
    }

    public void setDepositAmount(double depositAmount) {
        this.depositAmount = depositAmount;
    }

    public int getRentalId() {
        return rentalId;
    }

    public void setRentalId(int rentalId) {
        this.rentalId = rentalId;
    }

    public PostRentalRentResponse() {
    }

    @Override
    public String toString() {
        return "PostRentalRentResponse{" +
                "status='" + status + '\'' +
                ", balance=" + balance +
                ", depositAmount=" + depositAmount +
                ", rentalId=" + rentalId +
                '}';
    }
}
