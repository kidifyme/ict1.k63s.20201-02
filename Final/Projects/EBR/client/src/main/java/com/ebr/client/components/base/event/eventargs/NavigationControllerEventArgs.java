package com.ebr.client.components.base.event.eventargs;

public class NavigationControllerEventArgs extends EventArgs {
    public enum NavigationType {
        Forward,
        Backward
    }

    private NavigationType type;

    public NavigationControllerEventArgs(NavigationType type) {
        this.type = type;
    }

    public NavigationType getType() {
        return type;
    }

    public void setType(NavigationType type) {
        this.type = type;
    }
}
