package com.ebr.client.bean.bike;

import java.util.Date;

/**
 * Bean class representing a single-seat bike.
 */
public class SingleBike extends Bike {
    public SingleBike() {
    }

    public SingleBike(String name, String manufacturer, String stationId, String code) {
        super(name, manufacturer, stationId, code);
    }

    public SingleBike(String name, double weight, String licensePlate, Date manufactureDate, String manufacturer, double cost, String stationId, String code) {
        super(name, weight, licensePlate, manufactureDate, manufacturer, cost, stationId, code);
    }

    @Override
    public boolean match(Bike bike) {
        if (bike == null)
            return true;

        boolean res = super.match(bike);

        if (!res) {
            return false;
        }


        if (!(bike instanceof SingleBike))
            return false;
        return true;
    }
}
