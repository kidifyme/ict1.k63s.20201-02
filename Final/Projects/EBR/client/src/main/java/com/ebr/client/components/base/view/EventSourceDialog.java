package com.ebr.client.components.base.view;

import com.ebr.client.components.base.event.IEventHandler;
import com.ebr.client.components.base.event.IEventSource;
import com.ebr.client.components.base.event.eventargs.EventArgs;

import javax.swing.*;
import javax.swing.event.EventListenerList;
import java.awt.*;

/**
 * A convenient class that extends from <b>JDialog</b> and
 * implements <b>IEventSource</b>.
 * <p/>
 * Subclasses can emit events by invoking <b>emitEvent</b>.
 * <p/>
 * Note: <b>setVisible(Boolean)</b> method should <b>not</b> be called inside
 * subclasses' constructors. It is the task of this dialog's creator to invoke
 * <b>setVisible(Boolean)</b>. Any event handler registering must be done before
 * <b>setVisible(Boolean)</b>.
 *
 * @param <TEventArgs> a custom class that provides event information
 */
public class EventSourceDialog<TEventArgs extends EventArgs>
        extends JDialog
        implements IEventSource<TEventArgs> {

    protected EventListenerList listenerList = new EventListenerList();

    public EventSourceDialog(Frame owner, String title, boolean modal) {
        super(owner, title, modal);
    }

    @Override
    public void addEventHandler(IEventHandler<TEventArgs> eventHandler) {
        listenerList.add(IEventHandler.class, eventHandler);
    }

    @Override
    public void removeEventHandler(IEventHandler<TEventArgs> eventHandler) {
        listenerList.remove(IEventHandler.class, eventHandler);
    }

    protected void emitEvent(Object sender, TEventArgs eventArgs) {
        // Guaranteed to return a non-null array
        final Object[] listeners = listenerList.getListenerList();
        // Process the listeners last to first, notifying
        // those that are interested in this event
        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            if (listeners[i] == IEventHandler.class) {
                ((IEventHandler) listeners[i + 1]).onEvent(sender, eventArgs);
            }
        }
    }

    @Override
    public void setVisible(boolean b) {
        setLocationRelativeTo(null);
        super.setVisible(b);
    }
}
