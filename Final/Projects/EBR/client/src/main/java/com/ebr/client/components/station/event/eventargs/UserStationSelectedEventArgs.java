package com.ebr.client.components.station.event.eventargs;

import com.ebr.client.components.base.event.eventargs.EventArgs;

public class UserStationSelectedEventArgs extends EventArgs {
    private String stationId;

    public UserStationSelectedEventArgs(String stationId) {
        this.stationId = stationId;
    }

    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }
}
