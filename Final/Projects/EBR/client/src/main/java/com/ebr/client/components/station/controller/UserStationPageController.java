package com.ebr.client.components.station.controller;

import com.ebr.client.bean.station.Station;
import com.ebr.client.components.base.controller.PageController;
import com.ebr.client.components.base.datasource.IDataSource;
import com.ebr.client.components.base.event.IEventHandler;
import com.ebr.client.components.base.event.eventargs.DataActionEventArgs;
import com.ebr.client.components.base.view.ListPane;
import com.ebr.client.components.base.view.PagePane;
import com.ebr.client.components.base.view.SearchPane;
import com.ebr.client.components.station.datasource.StationDataSource;
import com.ebr.client.components.station.event.eventargs.UserStationSelectedEventArgs;
import com.ebr.client.components.station.view.StationSearchPane;
import com.ebr.client.components.station.view.UserStationListPane;
import com.ebr.client.components.station.view.UserStationPagePane;
import com.ebr.client.serverapi.IServerApi;

import java.util.ArrayList;
import java.util.Map;

public class UserStationPageController extends PageController<Station> {
    private final IServerApi serverApi;
    private StationDataSource stationDataSource;
    private IEventHandler<UserStationSelectedEventArgs> stationSelectedEventHandler;

    public UserStationPageController(IServerApi serverApi) {
        super();
        this.serverApi = serverApi;
    }

    public void setStationSelectedEventHandler(IEventHandler<UserStationSelectedEventArgs> eventHandler) {
        this.stationSelectedEventHandler = eventHandler;
    }

    @Override
    protected void onLinkingComponents() {
        stationDataSource = (StationDataSource) onCreateDataSource();

        // Search pane
        searchPane = onCreateSearchPane();
        searchPane.addEventHandler((sender, eventArgs) -> onSearchEvent(eventArgs.getSearchParams()));

        // List pane
        listPane = onCreateListPane(stationDataSource);
        listPane.addEventHandler((sender, eventArgs) -> onDataActionEvent(eventArgs));

        // Page pane
        pagePane = onCreatePagePane();
        pagePane.buildControls();
    }

    @Override
    protected IDataSource<Station> onCreateDataSource() {
        return new StationDataSource();
    }

    @Override
    protected SearchPane onCreateSearchPane() {
        return new StationSearchPane();
    }

    @Override
    protected ListPane<Station> onCreateListPane(IDataSource<Station> dataSource) {
        return new UserStationListPane(dataSource);
    }

    @Override
    protected PagePane<Station> onCreatePagePane() {
        return new UserStationPagePane(searchPane, listPane);
    }

    @Override
    protected void onDataActionEvent(DataActionEventArgs eventArgs) {
        String stationID = stationDataSource.getItem(eventArgs.getIndex()).getId();

        if (stationSelectedEventHandler != null) {
            stationSelectedEventHandler.onEvent(this, new UserStationSelectedEventArgs(stationID));
        }
    }

    @Override
    protected void onSearchEvent(Map<String, String> searchParams) {
        ArrayList<Station> result = serverApi.getStations(searchParams);
        stationDataSource.setSource(result);
    }
}
