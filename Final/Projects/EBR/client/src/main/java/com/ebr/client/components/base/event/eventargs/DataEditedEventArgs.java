package com.ebr.client.components.base.event.eventargs;

public class DataEditedEventArgs<TData> extends EventArgs {
    private TData data;

    public DataEditedEventArgs(TData data) {
        this.data = data;
    }

    public TData getData() {
        return data;
    }

    public void setData(TData data) {
        this.data = data;
    }
}
