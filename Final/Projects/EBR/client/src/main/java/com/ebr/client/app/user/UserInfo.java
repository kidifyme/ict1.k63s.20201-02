package com.ebr.client.app.user;

public class UserInfo {
    private static String creditCardNumber;

    public static String getCreditCardNumber() {
        return creditCardNumber;
    }

    public static void setCreditCardNumber(String creditCardNumber) {
        UserInfo.creditCardNumber = creditCardNumber;
    }
}
