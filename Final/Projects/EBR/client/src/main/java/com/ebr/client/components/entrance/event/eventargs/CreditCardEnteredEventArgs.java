package com.ebr.client.components.entrance.event.eventargs;

import com.ebr.client.components.base.event.eventargs.EventArgs;

public class CreditCardEnteredEventArgs extends EventArgs {
    private String creditCardNumber;

    public CreditCardEnteredEventArgs(String creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
    }

    public String getCreditCardNumber() {
        return creditCardNumber;
    }

    public void setCreditCardNumber(String creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
    }
}
