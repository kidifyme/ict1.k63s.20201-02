package com.ebr.client.components.bike.event.eventargs;

import com.ebr.client.components.base.event.eventargs.EventArgs;

public class DeleteActionEventArgs extends EventArgs {
    private int bikeIndex;

    public DeleteActionEventArgs(int bikeIndex) {
        setBikeIndex(bikeIndex);
    }

    public int getBikeIndex() {
        return bikeIndex;
    }

    public void setBikeIndex(int bikeIndex) {
        this.bikeIndex = bikeIndex;
    }
}
