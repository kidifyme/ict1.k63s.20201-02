package com.ebr.client.components.bike.controller;

import com.ebr.client.bean.bike.Bike;
import com.ebr.client.bean.bike.EBike;
import com.ebr.client.bean.bike.SingleBike;
import com.ebr.client.bean.bike.TwinBike;
import com.ebr.client.bean.station.Station;
import com.ebr.client.components.base.controller.PageController;
import com.ebr.client.components.base.datasource.IDataSource;
import com.ebr.client.components.base.event.eventargs.DataActionEventArgs;
import com.ebr.client.components.base.event.eventargs.DataEditDeleteEventArgs;
import com.ebr.client.components.base.event.eventargs.DataEditedEventArgs;
import com.ebr.client.components.base.view.ListPane;
import com.ebr.client.components.base.view.PagePane;
import com.ebr.client.components.base.view.SearchPane;
import com.ebr.client.components.bike.datasource.BikeDataSource;
import com.ebr.client.components.bike.view.*;
import com.ebr.client.serverapi.IServerApi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AdminBikePageController extends PageController<Bike> {
    private final IServerApi serverApi;
    private BikeDataSource bikeDataSource;

    private String containingStationId;

    private AdminBikeAddPane adminBikeAddPane;

    public AdminBikePageController(IServerApi serverApi) {
        super();
        this.serverApi = serverApi;
    }

    @Override
    protected void onLinkingComponents() {
        IDataSource<Bike> dataSource = onCreateDataSource();

        // Search pane
        searchPane = onCreateSearchPane();
        searchPane.addEventHandler((sender, eventArgs) -> onSearchEvent(eventArgs.getSearchParams()));

        // List pane
        listPane = onCreateListPane(dataSource);
        listPane.addEventHandler((sender, eventArgs) -> onDataActionEvent(eventArgs));

        adminBikeAddPane = onCreateAddPane();
        adminBikeAddPane.addEventHandler((sender, eventArgs) -> onAddActionEvent());

        // Page pane
        pagePane = onCreatePagePane();
        pagePane.buildControls();
    }

    protected void onAddActionEvent() {
        ArrayList<Station> stationList = serverApi.getStations(new HashMap<>());
        String[] stationIdList = null;

        if (stationList != null) {
            stationIdList = stationList.stream().map(station -> station.getId()).toArray(String[]::new);
        }

        // Show dialog
        BikeAddDialog bikeAddDialog = new BikeAddDialog(stationIdList);
        bikeAddDialog.addEventHandler((sender, eventArgs) -> {
            serverApi.addBike(eventArgs.getData());
            searchPane.fireSearchEvent();
        });
        bikeAddDialog.showDialog();
    }

    private AdminBikeAddPane onCreateAddPane() {
        return new AdminBikeAddPane();
    }

    @Override
    protected IDataSource<Bike> onCreateDataSource() {
        bikeDataSource = new BikeDataSource();
        return bikeDataSource;
    }

    @Override
    protected SearchPane onCreateSearchPane() {
        return new AdminBikeSearchPane();
    }

    @Override
    protected ListPane<Bike> onCreateListPane(IDataSource<Bike> dataSource) {
        return new AdminBikeListPane(dataSource);
    }

    @Override
    protected PagePane<Bike> onCreatePagePane() {
        return new AdminBikePagePane(searchPane, listPane, adminBikeAddPane);
    }

    @Override
    protected void onDataActionEvent(DataActionEventArgs eventArgs) {
        // TODO: show edit dialog / delete item
        if (eventArgs instanceof DataEditDeleteEventArgs) {
            DataEditDeleteEventArgs dataEditDeleteEventArgs = (DataEditDeleteEventArgs) eventArgs;
            System.out.println(dataEditDeleteEventArgs.getDataActionType());
            switch (dataEditDeleteEventArgs.getDataActionType()) {
                case EditAction:
                    editButtonPressed(dataEditDeleteEventArgs);
                    break;
                case DeleteAction:
                    deleteButtonPressed(dataEditDeleteEventArgs);
                    break;
            }
        }
    }

    private void deleteButtonPressed(DataEditDeleteEventArgs dataEditDeleteEventArgs) {
        Bike bike = bikeDataSource.getItem(dataEditDeleteEventArgs.getIndex());
        bikeDataSource.deleteItem(dataEditDeleteEventArgs.getIndex());
        serverApi.deleteBike(bike);
    }

    private void editButtonPressed(DataEditDeleteEventArgs dataEditDeleteEventArgs) {
        Bike bike = bikeDataSource.getItem(dataEditDeleteEventArgs.getIndex());
        if (bike instanceof SingleBike || bike instanceof TwinBike) {
            BikeEditDialog bikeEditDialog = new BikeEditDialog(bike);
            bikeEditDialog.addEventHandler((sender, eventArgs) -> onButtonPressed(eventArgs));
            bikeEditDialog.showDialog();
        } else if (bike instanceof EBike) {
            EBikeEditDialog eBikeEditDialog = new EBikeEditDialog(bike);
            eBikeEditDialog.addEventHandler((sender, eventArgs) -> onButtonPressed(eventArgs));
            eBikeEditDialog.showDialog();
        } else {
            System.out.println("No instance");
        }
        bikeDataSource.modifyItem(dataEditDeleteEventArgs.getIndex());
    }

    private void onButtonPressed(DataEditedEventArgs<Bike> eventArgs) {
        System.out.println(eventArgs.getData().getCode());
        serverApi.updateBike(eventArgs.getData());
    }

    @Override
    protected void onSearchEvent(Map<String, String> searchParams) {
        ArrayList<Bike> result = serverApi.getBikes(searchParams);
        bikeDataSource.setSource(result);
    }
}