package com.ebr.client.components.base.event.eventargs;

public class DataActionEventArgs extends EventArgs {
    protected int index;

    public DataActionEventArgs(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
