package com.ebr.client.serverapi.model;

public class GetRentalResponse {
    private int rentingTime;
    private int rentalId;
    private String bikeCode;
    private String bikeType;

    public String getBikeCode() {
        return bikeCode;
    }

    public void setBikeCode(String bikeCode) {
        this.bikeCode = bikeCode;
    }

    public String getBikeType() {
        return bikeType;
    }

    public void setBikeType(String bikeType) {
        this.bikeType = bikeType;
    }

    public int getRentingTime() {
        return rentingTime;
    }

    public void setRentingTime(int rentingTime) {
        this.rentingTime = rentingTime;
    }

    public int getRentalId() {
        return rentalId;
    }

    public void setRentalId(int rentalId) {
        this.rentalId = rentalId;
    }

    public GetRentalResponse() {
    }

}
