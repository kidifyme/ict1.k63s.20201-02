package com.ebr.client.components.base.event;

import com.ebr.client.components.base.event.eventargs.EventArgs;

import javax.swing.event.EventListenerList;

public class EventEmitterDelegate<TEventArgs extends EventArgs>
        implements IEventEmitter<TEventArgs> {
    private EventListenerList listenerList;

    public EventEmitterDelegate(EventListenerList listenerList) {
        this.listenerList = listenerList;
    }

    @Override
    public void emitEvent(Object sender, TEventArgs eventArgs) {
        // Guaranteed to return a non-null array
        final Object[] listeners = listenerList.getListenerList();
        // Process the listeners last to first, notifying
        // those that are interested in this event
        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            if (listeners[i] == IEventHandler.class) {
                ((IEventHandler) listeners[i + 1]).onEvent(sender, eventArgs);
            }
        }
    }
}
