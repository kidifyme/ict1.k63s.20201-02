package com.ebr.client.components.bike.view;

import com.ebr.client.bean.bike.Bike;
import com.ebr.client.bean.bike.EBike;
import com.ebr.client.bean.bike.SingleBike;
import com.ebr.client.bean.bike.TwinBike;
import com.ebr.client.components.base.datasource.IDataSource;
import com.ebr.client.components.base.event.eventargs.DataEditDeleteEventArgs;
import com.ebr.client.components.base.view.ItemPane;
import com.ebr.client.components.base.view.ListPane;

import javax.swing.*;
import java.awt.*;

public class AdminBikeListPane extends ListPane<Bike> {

    protected static class AdminBikeItemContainer extends ItemContainer<Bike> {

        @Override
        protected Component onCreateDataHandlingComponent() {

            JPanel panel = new JPanel();
            panel.setLayout(new FlowLayout(FlowLayout.LEFT));

            JButton editButton = new JButton("Edit");
            editButton.addActionListener(actionEvent -> {
                if (eventEmitter != null) {
                    eventEmitter.emitEvent(this, new DataEditDeleteEventArgs(DataEditDeleteEventArgs.DataActionType.EditAction, getIndex()));
                }

            });
            panel.add(editButton);

            JButton deleteButton = new JButton("Delete");
            deleteButton.addActionListener(actionEvent -> {
                if (eventEmitter != null) {
                    // TODO: Emit edit event
                    eventEmitter.emitEvent(this, new DataEditDeleteEventArgs(DataEditDeleteEventArgs.DataActionType.DeleteAction, getIndex()));
                }
            });
            panel.add(deleteButton);

            return panel;
        }
    }

    public AdminBikeListPane(IDataSource<Bike> dataSource) {
        super(dataSource);
    }

    @Override
    protected ItemPane<Bike> onCreateItemPane(Bike data) {
        if (data instanceof SingleBike) {
            return new SingleBikeItemPane();
        }
        if (data instanceof EBike) {
            return new EBikeItemPane();
        }
        if (data instanceof TwinBike) {
            return new TwinBikeItemPane();
        }

        return new BikeItemPane();
    }

    @Override
    protected ItemContainer<Bike> onCreateItemContainer() {
        return new AdminBikeItemContainer();
    }
}
