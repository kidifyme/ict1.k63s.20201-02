package com.ebr.client.components.station.controller;

import com.ebr.client.bean.station.Station;
import com.ebr.client.components.base.controller.PageController;
import com.ebr.client.components.base.datasource.IDataSource;
import com.ebr.client.components.base.event.eventargs.DataActionEventArgs;
import com.ebr.client.components.base.event.eventargs.DataEditDeleteEventArgs;
import com.ebr.client.components.base.event.eventargs.DataEditedEventArgs;
import com.ebr.client.components.base.view.ListPane;
import com.ebr.client.components.base.view.PagePane;
import com.ebr.client.components.base.view.SearchPane;
import com.ebr.client.components.station.datasource.StationDataSource;
import com.ebr.client.components.station.view.*;
import com.ebr.client.serverapi.IServerApi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AdminStationPageController extends PageController<Station> {
    private final IServerApi serverApi;
    private StationDataSource stationDataSource;

    private AdminStationAddPane adminStationAddPane;

    public AdminStationPageController(IServerApi serverApi) {
        super();
        this.serverApi = serverApi;
    }

    @Override
    protected void onLinkingComponents() {
        IDataSource<Station> dataSource = onCreateDataSource();

        // Search pane
        searchPane = onCreateSearchPane();
        searchPane.addEventHandler((sender, eventArgs) -> onSearchEvent(eventArgs.getSearchParams()));

        // List pane
        listPane = onCreateListPane(dataSource);
        listPane.addEventHandler((sender, eventArgs) -> onDataActionEvent(eventArgs));

        adminStationAddPane = onCreateAddPane();
        adminStationAddPane.addEventHandler((sender, eventArgs) -> onAddActionEvent());

        // Page pane
        pagePane = onCreatePagePane();
        pagePane.buildControls();
    }

    @Override
    protected IDataSource<Station> onCreateDataSource() {
        stationDataSource = new StationDataSource();
        return stationDataSource;
    }

    protected void onAddActionEvent() {
        ArrayList<Station> stationList = serverApi.getStations(new HashMap<>());
        String[] stationIdList = null;

        if (stationList != null) {
            stationIdList = stationList.stream().map(station -> station.getId()).toArray(String[]::new);
        }

        // Show dialog
        StationAddDialog stationAddDialog = new StationAddDialog(stationIdList);
        stationAddDialog.addEventHandler((sender, eventArgs) -> {
            serverApi.addStation(eventArgs.getData());
            searchPane.fireSearchEvent();
        });
        stationAddDialog.showDialog();
    }

    private AdminStationAddPane onCreateAddPane() {
        return new AdminStationAddPane();
    }

    @Override
    protected SearchPane onCreateSearchPane() {
        return new StationSearchPane();
    }

    @Override
    protected ListPane<Station> onCreateListPane(IDataSource<Station> dataSource) {
        return new AdminStationListPane(dataSource);
    }

    @Override
    protected void onDataActionEvent(DataActionEventArgs eventArgs) {
        //TODO: show edit dialog / delete item
        if (eventArgs instanceof DataEditDeleteEventArgs){
            DataEditDeleteEventArgs dataEditDeleteEventArgs = (DataEditDeleteEventArgs) eventArgs;
            switch (dataEditDeleteEventArgs.getDataActionType()){
                case EditAction:
                    editButtonPressed(dataEditDeleteEventArgs);;
                    break;
                case DeleteAction:
                    deleteButtonPressed(dataEditDeleteEventArgs);
                    break;
            }
        }
    }

    private void deleteButtonPressed(DataEditDeleteEventArgs dataEditDeleteEventArgs) {
        Station station = stationDataSource.getItem(dataEditDeleteEventArgs.getIndex());
        stationDataSource.deleteItem(dataEditDeleteEventArgs.getIndex());
        serverApi.deleteStation(station);
    }

    private void editButtonPressed(DataEditDeleteEventArgs dataEditDeleteEventArgs) {
        Station station = stationDataSource.getItem(dataEditDeleteEventArgs.getIndex());
        StationEditDialog stationEditDialog = new StationEditDialog(station);
        stationEditDialog.addEventHandler((sender, eventArgs) -> onButtonPressed(eventArgs));
        stationEditDialog.showDialog();

        stationDataSource.modifyItem(dataEditDeleteEventArgs.getIndex());
    }

    private void onButtonPressed(DataEditedEventArgs<Station> eventArgs) {
        serverApi.updateStation(eventArgs.getData());
    }

    @Override
    protected void onSearchEvent(Map<String, String> searchParams) {
        ArrayList<Station> result = serverApi.getStations(searchParams);
        stationDataSource.setSource(result);
    }

    @Override
    protected PagePane<Station> onCreatePagePane() {
        return new AdminStationPagePane(searchPane, listPane, adminStationAddPane);
    }
}
