package com.ebr.client.components.base.datasource;

import com.ebr.client.components.base.event.IEventSource;
import com.ebr.client.components.base.event.eventargs.DataSourceEventArgs;

/**
 * An abstraction over a data source. Provides simple <b>Observer</b> pattern
 * with <code>IEventSource</code>
 *
 * @param <T> a bean class type
 */
public interface IDataSource<T> extends IEventSource<DataSourceEventArgs> {
    int getSize();

    T getItem(int index);
}
