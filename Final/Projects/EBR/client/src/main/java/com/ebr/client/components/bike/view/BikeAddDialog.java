package com.ebr.client.components.bike.view;

import com.ebr.client.bean.bike.Bike;
import com.ebr.client.bean.bike.EBike;
import com.ebr.client.bean.bike.SingleBike;
import com.ebr.client.bean.bike.TwinBike;
import com.ebr.client.components.base.event.eventargs.DataEditedEventArgs;
import com.ebr.client.components.base.view.EditDialog;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BikeAddDialog extends EditDialog<Bike> {
    private JTextField codeField;
    private JTextField nameField;
    private JTextField weightField;
    private JTextField licenseField;
    private JTextField manufacturerField;
    private JTextField costField;
    private JTextField batteryField;
    private JTextField loadCyclesField;
    private JTextField timeRemainingField;

    private JComboBox<String> stationListBox;
    private JComboBox<String> typeListBox;
    private JComboBox<String> yearListBox;
    private JComboBox<String> monthListBox;
    private JComboBox<String> dayListBox;

    private JTextField errorCodeField;
    private JTextField errorNameField;
    private JTextField errorWeightField;
    private JTextField errorLicenseField;
    private JTextField errorManufacturerField;
    private JTextField errorCostField;
    private JTextField errorDateField;
    private JTextField errorBatteryField;
    private JTextField errorLoadCyclesField;
    private JTextField errorTimeRemainingField;

    private JButton saveButton;

    private final String[] stationIdList;

    public BikeAddDialog(String[] stationIdList) {
        super(null);
        this.setTitle("Add ");
        if (stationIdList == null) {
            stationIdList = new String[0];
        }

        this.stationIdList = stationIdList;
    }

    @Override
    protected void buildSaveButton() {
        saveButton = new JButton("Save");
        saveButton.addActionListener(e -> emitEvent(this, new DataEditedEventArgs<>(getNewData())));
        saveButton.addActionListener(actionEvent -> {
            BikeAddDialog.this.dispose();
        });

        c.gridx = 1;
        c.gridy = getLastRowIndex();
        getContentPane().add(saveButton, c);
    }

    @Override
    protected void buildControls() {
        c.fill = GridBagConstraints.HORIZONTAL;

        int row = getLastRowIndex();
        JLabel typeLabel = new JLabel("Type: ");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(typeLabel, c);
        String[] typeString = new String[]{"ebike", "singlebike", "twinbike"};
        typeListBox = new JComboBox<>(typeString);
        typeListBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println(typeListBox.getSelectedItem().toString());
                switch (typeListBox.getSelectedItem().toString()) {
                    case "ebike":
                        batteryField.setEditable(true);
                        loadCyclesField.setEditable(true);
                        timeRemainingField.setEditable(true);
                        break;
                    default:
                        batteryField.setEditable(false);
                        loadCyclesField.setEditable(false);
                        timeRemainingField.setEditable(false);
                        break;
                }
            }
        });
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(typeListBox, c);

        row = getLastRowIndex();
        JLabel codeLabel = new JLabel("Code: ");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(codeLabel, c);
        codeField = new JTextField(15);
        codeField.setText("bike4");
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(codeField, c);
        errorCodeField = new JTextField("", 10);
        errorCodeField.setForeground(Color.RED);
        errorCodeField.setEditable(false);
        c.gridx = 4;
        c.gridy = row;
        getContentPane().add(errorCodeField, c);
        validateNULL(codeField, errorCodeField);

        row = getLastRowIndex();
        JLabel nameLabel = new JLabel("Name: ");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(nameLabel, c);
        nameField = new JTextField(15);
        nameField.setText("bike4");
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(nameField, c);
        errorNameField = new JTextField("", 10);
        errorNameField.setForeground(Color.RED);
        errorNameField.setEditable(false);
        c.gridx = 4;
        c.gridy = row;
        getContentPane().add(errorNameField, c);
        validateNULL(nameField, errorNameField);

        row = getLastRowIndex();
        JLabel weightLabel = new JLabel("Weight: ");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(weightLabel, c);
        weightField = new JTextField(15);
        weightField.setText("10.0");
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(weightField, c);
        errorWeightField = new JTextField("", 10);
        errorWeightField.setForeground(Color.RED);
        errorWeightField.setEditable(false);
        c.gridx = 4;
        c.gridy = row;
        getContentPane().add(errorWeightField, c);
        validateDouble(weightField, errorWeightField);

        row = getLastRowIndex();
        JLabel licenseLabel = new JLabel("License plate: ");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(licenseLabel, c);
        licenseField = new JTextField(15);
        licenseField.setText("eco_bike4");
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(licenseField, c);
        errorLicenseField = new JTextField("", 10);
        errorLicenseField.setForeground(Color.RED);
        errorLicenseField.setEditable(false);
        c.gridx = 4;
        c.gridy = row;
        getContentPane().add(errorLicenseField, c);
        validateNULL(licenseField, errorLicenseField);

        row = getLastRowIndex();
        JLabel dateLabel = new JLabel("Manufacture Date: ");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(dateLabel, c);

        String[] yearString = new String[20];
        for (int i = 0; i < 20; i++) {
            yearString[i] = String.valueOf(2000 + i);
        }
        yearListBox = new JComboBox<>(yearString);
        yearListBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setOutputDate();
            }
        });
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(yearListBox, c);

        String[] monthString = new String[12];
        for (int i = 0; i < 12; i++) {
            monthString[i] = String.valueOf(1 + i);
        }
        monthListBox = new JComboBox<>(monthString);
        monthListBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setOutputDate();
            }
        });
        c.gridx = 2;
        c.gridy = row;
        getContentPane().add(monthListBox, c);

        String[] dayString = new String[31];
        for (int i = 0; i < 31; i++) {
            dayString[i] = String.valueOf(1 + i);
        }
        dayListBox = new JComboBox<>(dayString);
        dayListBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setOutputDate();
            }
        });
        c.gridx = 3;
        c.gridy = row;
        getContentPane().add(dayListBox, c);
        errorDateField = new JTextField("", 10);
        errorDateField.setForeground(Color.ORANGE);
        errorDateField.setEditable(false);
        c.gridx = 4;
        c.gridy = row;
        getContentPane().add(errorDateField, c);

        row = getLastRowIndex();
        JLabel manufacturerLabel = new JLabel("Manufacturer: ");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(manufacturerLabel, c);
        manufacturerField = new JTextField(15);
        manufacturerField.setText("Tandem");
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(manufacturerField, c);
        errorManufacturerField = new JTextField("", 10);
        errorManufacturerField.setForeground(Color.RED);
        errorManufacturerField.setEditable(false);
        c.gridx = 4;
        c.gridy = row;
        getContentPane().add(errorManufacturerField, c);
        validateNULL(manufacturerField, errorManufacturerField);

        row = getLastRowIndex();
        JLabel costLabel = new JLabel("Cost: ");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(costLabel, c);
        costField = new JTextField(15);
        costField.setText("100000.0");
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(costField, c);
        errorCostField = new JTextField("", 10);
        errorCostField.setForeground(Color.RED);
        errorCostField.setEditable(false);
        c.gridx = 4;
        c.gridy = row;
        getContentPane().add(errorCostField, c);
        validateDouble(costField, errorCostField);

        row = getLastRowIndex();
        JLabel stationLabel = new JLabel("Station: ");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(stationLabel, c);
        stationListBox = new JComboBox<>(stationIdList);
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(stationListBox, c);

        row = getLastRowIndex();
        JLabel batteryLabel = new JLabel("Batery: ");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(batteryLabel, c);
        batteryField = new JTextField(15);
        batteryField.setText("3");
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(batteryField, c);
        errorBatteryField = new JTextField("", 10);
        errorBatteryField.setForeground(Color.RED);
        errorBatteryField.setEditable(false);
        c.gridx = 4;
        c.gridy = row;
        getContentPane().add(errorBatteryField, c);
        validateInt(batteryField, errorBatteryField);

        row = getLastRowIndex();
        JLabel loadCyclesLabel = new JLabel("Load Cycles: ");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(loadCyclesLabel, c);
        loadCyclesField = new JTextField(15);
        loadCyclesField.setText("10");
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(loadCyclesField, c);
        errorLoadCyclesField = new JTextField("", 10);
        errorLoadCyclesField.setForeground(Color.RED);
        errorLoadCyclesField.setEditable(false);
        c.gridx = 4;
        c.gridy = row;
        getContentPane().add(errorLoadCyclesField, c);
        validateInt(loadCyclesField, errorLoadCyclesField);

        row = getLastRowIndex();
        JLabel timeLabel = new JLabel("Time remain: ");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(timeLabel, c);
        timeRemainingField = new JTextField(15);
        timeRemainingField.setText("3");
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(timeRemainingField, c);
        errorTimeRemainingField = new JTextField("", 10);
        errorTimeRemainingField.setForeground(Color.RED);
        errorTimeRemainingField.setEditable(false);
        c.gridx = 4;
        c.gridy = row;
        getContentPane().add(errorTimeRemainingField, c);
        validateDouble(timeRemainingField, errorTimeRemainingField);


    }

    private void setOutputDate() {
        String dateString = yearListBox.getSelectedItem() + "-" + monthListBox.getSelectedItem() + "-" + dayListBox.getSelectedItem();
        Date inputDate = null;
        try {
            inputDate = new SimpleDateFormat("yyyy-MM-dd").parse(dateString);
        } catch (ParseException parseException) {
            parseException.printStackTrace();
        }

        SimpleDateFormat dateFormatOutput = new SimpleDateFormat("yyyy-MM-dd");
        String x = dateFormatOutput.format(inputDate);
        errorDateField.setText(x);
    }

    @Override
    protected Bike getNewData() {
        // TODO: Handle twinBike and eBike
        String dateString;
        switch (typeListBox.getSelectedItem().toString()) {
            case "ebike":
                EBike eBike = new EBike();
                eBike.setCode(codeField.getText());
                eBike.setName(nameField.getText());
                eBike.setWeight(Float.parseFloat(weightField.getText()));
                eBike.setLicensePlate(licenseField.getText());
                dateString = yearListBox.getSelectedItem() + "-" + monthListBox.getSelectedItem() + "-" + dayListBox.getSelectedItem();
                try {
                    System.out.println(new SimpleDateFormat("yyyy-MM-dd").parse(dateString));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                try {
                    eBike.setManufactureDate(new SimpleDateFormat("yyyy-MM-dd").parse(dateString));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                eBike.setManufacturer(manufacturerField.getText());
                eBike.setCost(Double.parseDouble(costField.getText()));
                eBike.setStationId((String) stationListBox.getSelectedItem());
                eBike.setBatteryPercentage(Integer.parseInt(batteryField.getText()));
                eBike.setLoadCycles(Integer.parseInt(loadCyclesField.getText()));
                eBike.setTimeRemaining(Double.parseDouble(timeRemainingField.getText()));
                return eBike;

            case "singlebike":
                SingleBike singleBike = new SingleBike();
                singleBike.setCode(codeField.getText());
                singleBike.setName(nameField.getText());
                singleBike.setWeight(Float.parseFloat(weightField.getText()));
                singleBike.setLicensePlate(licenseField.getText());
                dateString = yearListBox.getSelectedItem() + "-" + monthListBox.getSelectedItem() + "-" + dayListBox.getSelectedItem();
                try {
                    singleBike.setManufactureDate(new SimpleDateFormat("yyyy-MM-dd").parse(dateString));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                singleBike.setManufacturer(manufacturerField.getText());
                singleBike.setCost(Double.parseDouble(costField.getText()));
                singleBike.setStationId((String) stationListBox.getSelectedItem());
                return singleBike;

            case "twinbike":
                TwinBike twinBike = new TwinBike();
                twinBike.setCode(codeField.getText());
                twinBike.setName(nameField.getText());
                twinBike.setWeight(Float.parseFloat(weightField.getText()));
                twinBike.setLicensePlate(licenseField.getText());
                dateString = yearListBox.getSelectedItem() + "-" + monthListBox.getSelectedItem() + "-" + dayListBox.getSelectedItem();
                try {
                    twinBike.setManufactureDate(new SimpleDateFormat("yyyy-MM-dd").parse(dateString));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                twinBike.setManufacturer(manufacturerField.getText());
                twinBike.setCost(Double.parseDouble(costField.getText()));
                twinBike.setStationId((String) stationListBox.getSelectedItem());
                return twinBike;

        }
        return t;
    }

    protected void validateNULL(JTextField textField, JTextField errorTextField) {
        textField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                checkNULL();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                checkNULL();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                checkNULL();
            }

            public void checkNULL() {
                if (!textField.getText().equals("")) {
                    errorTextField.setForeground(Color.ORANGE);
                    errorTextField.setText("Valid");
                    saveButton.setEnabled(true);
                } else {
                    errorTextField.setForeground(Color.red);
                    errorTextField.setText("Should not NULL");
                    saveButton.setEnabled(false);
                }
            }
        });
    }

    protected void validateDouble(JTextField textField, JTextField errorTextField) {
        textField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                checkDouble();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                checkDouble();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                checkDouble();
            }

            public void checkDouble() {
                try {
                    Double i = Double.parseDouble(textField.getText());
                    errorTextField.setText("Valid");
                    errorTextField.setForeground(Color.ORANGE);
                    saveButton.setEnabled(true);
                } catch (NumberFormatException e1) {
                    errorTextField.setText("Double required");
                    saveButton.setEnabled(false);
                }
            }
        });
    }

    protected void validateInt(JTextField textField, JTextField errorTextField) {
        textField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                checkDouble();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                checkDouble();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                checkDouble();
            }

            public void checkDouble() {
                try {
                    int i = Integer.parseInt(textField.getText());
                    errorTextField.setText("Valid");
                    errorTextField.setForeground(Color.ORANGE);
                    saveButton.setEnabled(true);
                } catch (NumberFormatException e1) {
                    errorTextField.setText("Integer required");
                    saveButton.setEnabled(false);
                }
            }
        });
    }
}
