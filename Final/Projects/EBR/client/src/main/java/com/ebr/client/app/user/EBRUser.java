package com.ebr.client.app.user;

import com.ebr.client.components.entrance.view.EnterCreditCardDialog;
import com.ebr.client.serverapi.ServerAPI;

import javax.swing.*;
import java.awt.event.WindowEvent;

/**
 * Main entry point for user app
 */
public class EBRUser extends JFrame {
    public static final int WINDOW_WIDTH = 800;
    public static final int WINDOW_HEIGHT = 550;

    public EBRUser() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Eco bike rental (User)");
        setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        setLocationRelativeTo(null);

        EnterCreditCardDialog entranceDialog = new EnterCreditCardDialog(this, new ServerAPI());
        entranceDialog.addEventHandler((sender, eventArgs) -> {
            UserInfo.setCreditCardNumber(eventArgs.getCreditCardNumber());
        });

        entranceDialog.showDialog();

        if (UserInfo.getCreditCardNumber() != null) {
            EBRUserController controller = new EBRUserController();
            setContentPane(controller.getRootPanel());
            this.setVisible(true);
        } else {
            dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
        }
    }

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }

        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new EBRUser();
            }
        });
    }
}
