package com.ebr.client.components.station.view;

import com.ebr.client.bean.station.Station;
import com.ebr.client.components.base.datasource.IDataSource;
import com.ebr.client.components.base.event.eventargs.DataActionEventArgs;
import com.ebr.client.components.base.view.ItemPane;
import com.ebr.client.components.base.view.ListPane;

import javax.swing.*;
import java.awt.*;

public class UserStationListPane extends ListPane<Station> {
    protected static class UserStationItemContainer extends ItemContainer<Station> {

        @Override
        protected Component onCreateDataHandlingComponent() {
            JPanel panel = new JPanel();
            panel.setLayout(new FlowLayout(FlowLayout.LEFT));

            JButton detailsButton = new JButton("Details");
            detailsButton.addActionListener(actionEvent -> {
                if (eventEmitter != null) {
                    eventEmitter.emitEvent(this, new DataActionEventArgs(getIndex()));
                }
            });

            panel.add(detailsButton);

            return panel;
        }
    }

    public UserStationListPane(IDataSource<Station> dataSource) {
        super(dataSource);
    }

    @Override
    protected ItemPane<Station> onCreateItemPane(Station data) {
        return new StationItemPane();
    }

    @Override
    protected ItemContainer<Station> onCreateItemContainer() {
        return new UserStationItemContainer();
    }
}
