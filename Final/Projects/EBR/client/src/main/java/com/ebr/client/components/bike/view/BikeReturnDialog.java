package com.ebr.client.components.bike.view;

import com.ebr.client.components.base.event.eventargs.EventArgs;
import com.ebr.client.components.base.view.EventSourceDialog;
import com.ebr.client.serverapi.model.PostRentalReturnResponse;

import javax.swing.*;
import java.awt.*;

public class BikeReturnDialog extends EventSourceDialog<EventArgs> {

    private GridBagLayout layout;
    private GridBagConstraints c = new GridBagConstraints();

    private JTextField statusField;
    private JTextField balanceField;
    private JTextField depositAmountField;
    private JTextField currentRentFeeField;
    private JTextField finalBalanceField;

    public BikeReturnDialog(PostRentalReturnResponse postRentalReturnResponse) {
        super((Frame) null, "Return", true);

        setContentPane(new JPanel());
        layout = new GridBagLayout();
        getContentPane().setLayout(layout);

        this.buildControls(postRentalReturnResponse);
        JButton confirmButton = new JButton("Confirm");
        confirmButton.addActionListener(e -> {
            emitEvent(this, new EventArgs());
            BikeReturnDialog.this.dispose();
        });

        // Validate rental request
        if (postRentalReturnResponse.getStatus().equals("ok")) {
            confirmButton.addActionListener(e -> {
                emitEvent(this, new EventArgs());
                this.dispose();
            });
        } else {
            confirmButton.setEnabled(false);
        }

        c.gridx = 1;
        c.gridy = getLastRowIndex();
        getContentPane().add(confirmButton, c);

        this.pack();
    }

    private int getLastRowIndex() {
        layout.layoutContainer(getContentPane());
        int[][] dim = layout.getLayoutDimensions();
        int rows = dim[1].length;
        return rows;
    }

    private void buildControls(PostRentalReturnResponse postRentalReturnResponse) {
        int row = getLastRowIndex();

        JLabel statusLabel = new JLabel("Status: ");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(statusLabel, c);
        statusField = new JTextField(15);
        statusField.setText(postRentalReturnResponse.getStatus());
        statusField.setEditable(false);
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(statusField, c);

        row = getLastRowIndex();
        JLabel balanceLabel = new JLabel("Balance: ");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(balanceLabel, c);
        balanceField = new JTextField(15);
        balanceField.setText(String.valueOf(postRentalReturnResponse.getBalance()));
        balanceField.setEditable(false);
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(balanceField, c);

        row = getLastRowIndex();
        JLabel depositAmountLabel = new JLabel("Deposit amount: ");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(depositAmountLabel, c);
        depositAmountField = new JTextField(15);
        depositAmountField.setText(String.valueOf(postRentalReturnResponse.getDepositAmount()));
        depositAmountField.setEditable(false);
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(depositAmountField, c);

        row = getLastRowIndex();
        JLabel currentRentFeeLabel = new JLabel("Current rent fee: ");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(currentRentFeeLabel, c);
        currentRentFeeField = new JTextField(15);
        currentRentFeeField.setText(String.valueOf(postRentalReturnResponse.getCurrentRentFee()));
        currentRentFeeField.setEditable(false);
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(currentRentFeeField, c);

        row = getLastRowIndex();
        JLabel finalBalanceLabel = new JLabel("Balance after return bike: ");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(finalBalanceLabel, c);
        finalBalanceField = new JTextField(15);
        finalBalanceField.setText(String.valueOf(postRentalReturnResponse.getBalance() + postRentalReturnResponse.getDepositAmount() - postRentalReturnResponse.getCurrentRentFee()));
        finalBalanceField.setEditable(false);
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(finalBalanceField, c);

    }
}
