package com.ebr.client.components.base.view;

import javax.swing.*;

/**
 * A container composed of a {@link com.ebr.client.components.base.view.SearchPane}
 * and a {@link com.ebr.client.components.base.view.ListPane}.
 * <p/>
 * Subclasses can add more components to this container.
 *
 * @param <T> a bean class type
 */
public class PagePane<T> extends JPanel {
    protected SearchPane searchPane;
    protected ListPane<T> listPane;

    protected SpringLayout layout;

    protected PagePane() {
        layout = new SpringLayout();
        setLayout(layout);
    }

    public PagePane(SearchPane searchPane, ListPane<T> listPane) {
        this();
        this.searchPane = searchPane;
        this.listPane = listPane;
    }

    public void buildControls() {
        layout.putConstraint(SpringLayout.WEST, searchPane, 5, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, searchPane, 5, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.EAST, searchPane, -5, SpringLayout.EAST, this);
        add(searchPane);

        layout.putConstraint(SpringLayout.WEST, listPane, 5, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, listPane, 5, SpringLayout.SOUTH, searchPane);
        layout.putConstraint(SpringLayout.EAST, listPane, -5, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.SOUTH, listPane, -5, SpringLayout.SOUTH, this);
        add(listPane);
    }
}
