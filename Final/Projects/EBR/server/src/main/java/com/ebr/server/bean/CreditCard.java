package com.ebr.server.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonTypeName;

import java.util.Date;

@JsonTypeName("creditCard")
public class CreditCard {
    private String cardNumber;
    private String password;
    private String cardOwner;
    private double balance;
    private String issuingBank;
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date expirationDate;

    public CreditCard(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public CreditCard(String cardNumber, String password) {
        this.cardNumber = cardNumber;
        this.password = password;
    }

    public CreditCard() {
    }

    public CreditCard(String cardNumber, String password, String cardOwner, int balance, String issuingBank, Date expirationDate) {
        this.cardNumber = cardNumber;
        this.password = password;
        this.cardOwner = cardOwner;
        this.balance = balance;
        this.issuingBank = issuingBank;
        this.expirationDate = expirationDate;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCardOwner() {
        return cardOwner;
    }

    public void setCardOwner(String cardOwner) {
        this.cardOwner = cardOwner;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getIssuingBank() {
        return issuingBank;
    }

    public void setIssuingBank(String issuingBank) {
        this.issuingBank = issuingBank;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public boolean match(CreditCard creditCard){
        if (creditCard == null)
            return true;


        if (creditCard.cardNumber != null && !creditCard.cardNumber.equals("") && !this.cardNumber.equals(creditCard.cardNumber)) {
            return false;
        }
        if (creditCard.password != null && !creditCard.password.equals("") && !this.password.equals(creditCard.password)) {
            return false;
        }
        return true;
    }
}
