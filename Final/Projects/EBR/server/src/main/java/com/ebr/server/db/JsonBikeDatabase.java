package com.ebr.server.db;

import java.util.ArrayList;

import com.ebr.server.bean.*;
import com.ebr.server.db.seed.Seed;

public class JsonBikeDatabase<Bikes> implements IBikeDatabase {
    private static IBikeDatabase singleton = new JsonBikeDatabase();

    private ArrayList<Bike> bikes = Seed.singleton().getBikes();
    private ArrayList<Station> stations = Seed.singleton().getStations();

    public JsonBikeDatabase() {
    }

    public static IBikeDatabase singleton() {
        return singleton;
    }

    @Override
    public void addBikeToStation(Station station, Bike bike) {
        if (bike instanceof SingleBike) station.setNumOfSingleBikes(station.getNumOfSingleBikes() + 1);
        else if (bike instanceof TwinBike) station.setNumOfTwinBikes(station.getNumOfTwinBikes() + 1);
        else station.setNumOfEBikes(station.getNumOfEBikes() + 1);
        station.setNumOfEmptyDocks(station.getNumOfEmptyDocks() - 1);
    }

    @Override
    public void deleteBikeFromStation(Station station, Bike bike) {
        if (bike instanceof SingleBike) station.setNumOfSingleBikes(station.getNumOfSingleBikes() - 1);
        else if (bike instanceof TwinBike) station.setNumOfTwinBikes(station.getNumOfTwinBikes() - 1);
        else station.setNumOfEBikes(station.getNumOfEBikes() - 1);
        station.setNumOfEmptyDocks(station.getNumOfEmptyDocks() + 1);
    }


    @Override
    public ArrayList<Bike> searchBike(Bike bike, String[] kindArray) {
        ArrayList<Bike> res = new ArrayList<Bike>();
        for (Bike b : bikes) {
            for (String kind : kindArray) {
                if ((kind.equals("singlebike") && b instanceof SingleBike) || (kind.equals("twinbike") && b instanceof TwinBike) || (kind.equals("ebike") && b instanceof EBike))
                    if (b.match(bike)) {
                        res.add(b);
                    }
            }
        }
        return res;
    }

    @Override
    public Bike addBike(Bike bike) {
        for (Bike m : bikes)
            if (m.equals(bike))
                return null;

        for (Station s : stations)
            if (s.getId().equals(bike.getStationId())){
                addBikeToStation(s, bike);
                bikes.add(bike);
                return bike;
            }

        return null;
    }

    @Override
    public Bike updateBike(String code, Bike bike) {
        for (Bike m : bikes) {
            if (m.getCode().equals(code)) {
                bikes.remove(m);
                bikes.add(bike);
                for (Station station : stations) {
                    if (station.getId().equals(m.getStationId()))
                        deleteBikeFromStation(station, m);
                    if (station.getId().equals(bike.getStationId()))
                        addBikeToStation(station, bike);
                }
                return bike;
            }
        }
        return null;
    }

    @Override
    public boolean deleteBike(String code) {
        for (Bike m : bikes)
            if (m.getCode().equals(code)) {
                bikes.remove(m);
                for (Station s : stations)
                    if (s.getId().equals(m.getStationId()))
                        deleteBikeFromStation(s, m);
                return true;
            }

        return false;
    }

}
