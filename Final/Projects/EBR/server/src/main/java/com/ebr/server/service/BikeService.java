package com.ebr.server.service;

import java.util.ArrayList;

import javax.ws.rs.*;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import com.ebr.server.bean.Bike;
import com.ebr.server.db.IBikeDatabase;
import com.ebr.server.db.JsonBikeDatabase;

@Path("/bikes")
public class BikeService {

    private final IBikeDatabase bikeDatabase;

    public BikeService() {
        bikeDatabase = JsonBikeDatabase.singleton();
    }


    @GET
    @Path("")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Bike> getBikes(@QueryParam("name") String name, @QueryParam("manufacturer") String manufacturer, @QueryParam("stationId") String stationId,
                                    @QueryParam("kind") String kind  ,@QueryParam("code") String code, @QueryParam("licensePlate") String licensePlate) {
        if(kind != null){
            ArrayList<Bike> res = new ArrayList<>();
            String[] kindArray = kind.split(",");
            Bike bike = new Bike(name,manufacturer,stationId,code, licensePlate);

            return bikeDatabase.searchBike(bike,kindArray);
        }
        return new ArrayList<>();
    }

    @PUT
    @Path("/{code}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Bike updateBike(@PathParam("code") String code, Bike bike) {
        return bikeDatabase.updateBike(code,bike);
    }

    @POST
    @Path("")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Bike addBike(Bike bike) {
        return bikeDatabase.addBike(bike);
    }

    @DELETE
    @Path("/{code}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public boolean deleteBike(@PathParam("code") String code) {
        return bikeDatabase.deleteBike(code);
    }

}