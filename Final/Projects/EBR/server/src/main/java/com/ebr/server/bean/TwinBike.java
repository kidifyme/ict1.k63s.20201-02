package com.ebr.server.bean;

import java.util.Date;

public class TwinBike extends Bike {
    public TwinBike() {
    }

    public TwinBike(String name, double weight, String licensePlate, Date manufactureDate, String manufacturer, double cost, String stationId, String code) {
        super(name, weight, licensePlate, manufactureDate, manufacturer, cost, stationId, code);
    }

    public TwinBike(String name, String manufacturer, String stationId, String code, String licensePlate) {
        super(name, manufacturer, stationId, code, licensePlate);
    }

//    @Override
//    public boolean match(Bike bike) {
//        if (bike == null)
//            return true;
//
//        boolean res = super.match(bike);
//
//        if (!res) {
//            return false;
//        }
//
//
//        if (!(bike instanceof TwinBike))
//            return false;
//        return true;
//    }
}
