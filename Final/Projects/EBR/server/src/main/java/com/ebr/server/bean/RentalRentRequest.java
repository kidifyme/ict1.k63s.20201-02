package com.ebr.server.bean;

public class RentalRentRequest {
    private boolean preview;
    private String bikeCode;
    private String cardNumber;

    public RentalRentRequest(boolean preview, String bikeCode, String cardNumber) {
        this.preview = preview;
        this.bikeCode = bikeCode;
        this.cardNumber = cardNumber;
    }

    public boolean isPreview() {
        return preview;
    }

    public void setPreview(boolean preview) {
        this.preview = preview;
    }

    public String getBikeCode() {
        return bikeCode;
    }

    public void setBikeCode(String bikeCode) {
        this.bikeCode = bikeCode;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public RentalRentRequest() {
    }
}
