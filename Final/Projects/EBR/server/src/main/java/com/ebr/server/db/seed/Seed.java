package com.ebr.server.db.seed;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import com.ebr.server.bean.CreditCard;
import com.ebr.server.bean.Rental;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ebr.server.bean.Bike;
import com.ebr.server.bean.Station;

public class Seed {
    private ArrayList<Bike> bikes;
    private ArrayList<Station> stations;
    private ArrayList<CreditCard> creditCards;
    private ArrayList<Rental> rentals;

    private static Seed singleton = new Seed();

    private Seed() {
        start();
    }

    public static Seed singleton() {
        return singleton;
    }

    private void start() {
        bikes = new ArrayList<Bike>();
        stations = new ArrayList<Station>();
        creditCards = new ArrayList<CreditCard>();
        rentals = new ArrayList<Rental>();
        bikes.addAll(generateBikeDataFromFile(new File(getClass().getResource("/db/bike.json").getPath()).toString()));
        stations.addAll(generateStationDataFromFile(new File(getClass().getResource("/db/station.json").getPath()).toString()));
        creditCards.addAll(generateCreditCardDataFromFile(new File(getClass().getResource("/db/creditCard.json").getPath()).toString()));
        rentals.addAll(generateRentalDataFromFile(new File(getClass().getResource("/db/rental.json").getPath()).toString()));
    }

    private ArrayList<? extends Bike> generateBikeDataFromFile(String filePath) {
        ArrayList<? extends Bike> res = new ArrayList<Bike>();
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        String json = FileReader.read(filePath);
        try {
            mapper.setDateFormat(new SimpleDateFormat("yyyy-dd-MM"));
            res = mapper.readValue(json, new TypeReference<ArrayList<Bike>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Invalid JSON input data from " + filePath);
        }

        return res;
    }

    private ArrayList<? extends Station> generateStationDataFromFile(String filePath) {
        ArrayList<? extends Station> res = new ArrayList<Station>();
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        String json = FileReader.read(filePath);
        try {
            res = mapper.readValue(json, new TypeReference<ArrayList<Station>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Invalid JSON input data from " + filePath);
        }

        return res;
    }

    private ArrayList<? extends CreditCard> generateCreditCardDataFromFile(String filePath) {
        ArrayList<? extends CreditCard> res = new ArrayList<CreditCard>();
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        String json = FileReader.read(filePath);
        try {
            res = mapper.readValue(json, new TypeReference<ArrayList<CreditCard>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Invalid JSON input data from " + filePath);
        }

        return res;
    }

    private ArrayList<? extends Rental> generateRentalDataFromFile(String filePath) {
        ArrayList<? extends Rental> res = new ArrayList<Rental>();
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        String json = FileReader.read(filePath);
        try {
            res = mapper.readValue(json, new TypeReference<ArrayList<Rental>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Invalid JSON input data from " + filePath);
        }

        return res;
    }

    public ArrayList<Bike> getBikes() {
        return bikes;
    }

    public ArrayList<Station> getStations() {
        return stations;
    }

    public ArrayList<CreditCard> getCreditCards(){ return creditCards;}

    public ArrayList<Rental> getRentals(){ return rentals;}


    public static void main(String[] args) {
        Seed seed = new Seed();
        seed.start();
    }
}
