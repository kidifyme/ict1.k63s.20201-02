package com.ebr.server.db;

import com.ebr.server.bean.*;
import com.ebr.server.db.seed.Seed;

import java.util.ArrayList;
import java.util.Date;

public class JsonRentalDatabase implements IRentalDatabase {
    private static IRentalDatabase singleton = new JsonRentalDatabase();
    private CalculateCost calculateCost = new CalculateCost();
    private JsonBikeDatabase jsonBikeDatabase = new JsonBikeDatabase();

    private ArrayList<Rental> rentals = Seed.singleton().getRentals();
    private ArrayList<Bike> bikes = Seed.singleton().getBikes();
    private ArrayList<CreditCard> creditCards = Seed.singleton().getCreditCards();
    private ArrayList<Station> stations = Seed.singleton().getStations();

    static int id = 1;

    private JsonRentalDatabase() {
    }

    public static IRentalDatabase singleton() {
        return singleton;
    }

    @Override
    public GetRentalResponse getRental(Rental rental) {
        Rental tempRental = findInRentalList(rental.getCardNumber());
        if (tempRental != null) {
            Bike bike = findBikeByCode(tempRental.getBikeCode());
            GetRentalResponse getRentalResponse = new GetRentalResponse();
            getRentalResponse.setRentalId(tempRental.getRentalId());
            getRentalResponse.setBikeCode(bike.getCode());
            if(bike instanceof SingleBike)  getRentalResponse.setBikeType("single bike");
            else if(bike instanceof TwinBike)    getRentalResponse.setBikeType("twin bike");
            else getRentalResponse.setBikeType("electronic bike");
            Date currentTime = new Date();
            getRentalResponse.setRentingTime((int) (currentTime.getTime() - tempRental.getStartTime().getTime()) / 60000);
            return getRentalResponse;
        }
        return null;
    }

    @Override
    public PostRentalRentResponse addRental(RentalRentRequest rentalRentRequest) {
        PostRentalRentResponse postRentalRentResponse = new PostRentalRentResponse();
        Rental rental = new Rental(rentalRentRequest.getCardNumber());
        Bike bike = findBikeByCode(rentalRentRequest.getBikeCode());
        CreditCard creditCard = findCreditCardByNumber(rentalRentRequest.getCardNumber());

        if(findInRentalList(rentalRentRequest.getCardNumber()) != null) return null; // check if this credit card is renting a bike
        if (creditCard == null || bike == null) return null;    // check validation

        double depositAmount = calculateCost.getDepositAmount(bike);
        double balance = findCreditCardByNumber(rentalRentRequest.getCardNumber()).getBalance();

        postRentalRentResponse.setDepositAmount(depositAmount);
        postRentalRentResponse.setBalance(balance);

        if (depositAmount <= balance)
            postRentalRentResponse.setStatus("ok");
        else postRentalRentResponse.setStatus("error");

        if (!rentalRentRequest.isPreview()) {   // preview = false -> confirm rental, update info of bike, station, creditCard, rental
            if (findInRentalList(rental.getCardNumber()) == null) {
                rental.setRentalId(id);
                rental.setBikeCode(rentalRentRequest.getBikeCode());
                rental.setCardNumber(rentalRentRequest.getCardNumber());
                rental.setStartTime(new Date());
                id++;
                rentals.add(rental);
                jsonBikeDatabase.deleteBikeFromStation(findStationById(bike.getStationId()), bike);
                bike.setStationId("none");  // set StationId of bike to none
                creditCard.setBalance(creditCard.getBalance() - calculateCost.getDepositAmount(bike)); // deduce deposit
            }
            postRentalRentResponse.setRentalId(findInRentalList(rental.getCardNumber()).getRentalId());
        }
        return postRentalRentResponse;
    }

    @Override
    public PostRentalReturnResponse returnBike(RentalReturnRequest rentalReturnRequest) {
        PostRentalReturnResponse postRentalReturnResponse = new PostRentalReturnResponse();
        CreditCard creditCard = findCreditCardByNumber(rentalReturnRequest.getCardNumber());
        Station station = findStationById(rentalReturnRequest.getStationId());
        Rental rental = findInRentalList(rentalReturnRequest.getCardNumber());

        // check validation
        if (rental == null) {
            postRentalReturnResponse.setStatus("error");
            return postRentalReturnResponse;
        }

        if (station == null || creditCard == null) return null;

        Bike bike = findBikeByCode(rental.getBikeCode());

        postRentalReturnResponse.setStatus("ok");
        postRentalReturnResponse.setBalance(creditCard.getBalance());
        postRentalReturnResponse.setDepositAmount(calculateCost.getDepositAmount(bike));
        Date now = new Date();
        double rentingTime = now.getTime() - rental.getStartTime().getTime();
        postRentalReturnResponse.setCurrentRentFee(calculateCost.getCost(bike, rentingTime / 60000));

        if (!rentalReturnRequest.isPreview()) {       // preview = false => confirm return
            creditCard.setBalance(creditCard.getBalance() - postRentalReturnResponse.getCurrentRentFee() +
                    calculateCost.getDepositAmount(bike)); // update credit card balance
            bike.setStationId(station.getId());
            jsonBikeDatabase.addBikeToStation(station, bike);
            rentals.remove(rental);     // remove current rental from db
        }
        return postRentalReturnResponse;
    }

    public Bike findBikeByCode(String codeBike) {
        for (Bike bike : bikes)
            if (bike.getCode().equals(codeBike))
                return bike;
        return null;
    }

    public Station findStationById(String stationId) {
        for (Station station : stations)
            if (station.getId().equals(stationId))
                return station;
        return null;
    }

    public CreditCard findCreditCardByNumber(String cardNumber) {
        for (CreditCard creditCard : creditCards)
            if (creditCard.getCardNumber().equals(cardNumber))
                return creditCard;
        return null;
    }

    public Rental findInRentalList(String cardNumber) {
        for (Rental r : rentals)
            if (r.getCardNumber().equals(cardNumber))
                return r;
        return null;
    }
}
