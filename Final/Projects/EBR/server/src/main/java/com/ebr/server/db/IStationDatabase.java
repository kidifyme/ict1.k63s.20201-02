package com.ebr.server.db;

import java.util.ArrayList;

import com.ebr.server.bean.Station;

public interface IStationDatabase {
    public ArrayList<Station> searchStation(Station station);
    public Station updateStation(String id, Station station);
    public Station addStation(Station station);
    public boolean deleteStation(String id);
}
