package com.ebr.server.bean;

public class PostRentalReturnResponse {
    private String status;
    private double balance;
    private double depositAmount;
    private double currentRentFee;

    public PostRentalReturnResponse() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double getDepositAmount() {
        return depositAmount;
    }

    public void setDepositAmount(double depositAmount) {
        this.depositAmount = depositAmount;
    }

    public double getCurrentRentFee() {
        return currentRentFee;
    }

    public void setCurrentRentFee(double currentRentFee) {
        this.currentRentFee = currentRentFee;
    }
}
