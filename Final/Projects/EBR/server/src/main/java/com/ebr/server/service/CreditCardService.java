package com.ebr.server.service;

import com.ebr.server.bean.CreditCard;
import com.ebr.server.db.ICreditCardDatabase;
import com.ebr.server.db.JsonCreditCardDatabase;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;

@Path("/cards")
public class CreditCardService {
    private final ICreditCardDatabase creditCardDatabase;

    public CreditCardService() {
        creditCardDatabase = JsonCreditCardDatabase.singleton();
    }

    @POST
    @Path("")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public void changeBalance(@QueryParam("cardNumber") String cardNumber,
                            @QueryParam("amount") int amount,
                            @QueryParam("action") String action) {
        CreditCard creditCard = new CreditCard(cardNumber);
        if(action.equals("plus")) creditCardDatabase.add(creditCard,amount);
        if(action.equals("minus")) creditCardDatabase.deduct(creditCard,amount);
    }

    @GET
    @Path("")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<CreditCard> getCreditCard(@QueryParam("cardNumber") String cardNumber) {
        CreditCard creditCard = new CreditCard(cardNumber);
        return creditCardDatabase.searchCreditCard(creditCard);
    }
}
