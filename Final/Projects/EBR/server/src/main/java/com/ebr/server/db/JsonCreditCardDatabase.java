package com.ebr.server.db;



import com.ebr.server.bean.CreditCard;
import com.ebr.server.db.seed.Seed;

import java.util.ArrayList;

public class JsonCreditCardDatabase implements ICreditCardDatabase {
    private static ICreditCardDatabase singleton = new JsonCreditCardDatabase();

    private ArrayList<CreditCard> creditCards = Seed.singleton().getCreditCards();
    public JsonCreditCardDatabase(){}
    public static ICreditCardDatabase singleton() {
        return singleton;
    }
    @Override
    public ArrayList<CreditCard> searchCreditCard(CreditCard creditCard){
        ArrayList<CreditCard> res = new ArrayList<CreditCard>();
        for (CreditCard c: creditCards) {
            if (c.match(creditCard)) {
                res.add(c);
            }
        }
        return res;
    }

    @Override
    public void deduct(CreditCard creditCard, int amount){
        if(creditCard.getBalance() >= amount)
            for(CreditCard c: creditCards)
                if(c.match(creditCard))
                    c.setBalance(c.getBalance() - amount);
    }


    @Override
    public void add(CreditCard creditCard, int amount){
        for(CreditCard c: creditCards)
            if(c.match(creditCard))
                c.setBalance(c.getBalance() + amount);
    }

    @Override
    public double checkBalance(CreditCard creditCard){
        return creditCard.getBalance();
    }
}
