package com.ebr.server.db;

import java.util.ArrayList;

import com.ebr.server.bean.Bike;
import com.ebr.server.bean.Station;

public interface IBikeDatabase {
    public ArrayList<Bike> searchBike(Bike bike, String[] kindArray);
    public Bike updateBike(String code, Bike bike);
    public Bike addBike(Bike bike);
    public boolean deleteBike(String code);
    public void addBikeToStation(Station station, Bike bike);
    public void deleteBikeFromStation(Station station, Bike bike);
}
