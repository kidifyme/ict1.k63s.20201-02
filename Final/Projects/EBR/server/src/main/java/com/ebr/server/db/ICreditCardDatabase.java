package com.ebr.server.db;

import com.ebr.server.bean.CreditCard;

import java.util.ArrayList;

public interface ICreditCardDatabase {
    public ArrayList<CreditCard> searchCreditCard(CreditCard creditCard);
    public void deduct(CreditCard creditCard, int amount);
    public void add(CreditCard creditCard, int amount);
    public double checkBalance(CreditCard creditCard);
}
