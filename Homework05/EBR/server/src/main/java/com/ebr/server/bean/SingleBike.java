package com.ebr.server.bean;

import java.util.Date;

public class SingleBike extends Bike {
    public SingleBike() {
    }

    public SingleBike(String name, String manufacturer, String stationId, String code, String licensePlate) {
        super(name, manufacturer, stationId, code, licensePlate);
    }

    public SingleBike(String name, double weight, String licensePlate, Date manufactureDate, String manufacturer, double cost, String stationId, String code) {
        super(name, weight, licensePlate, manufactureDate, manufacturer, cost, stationId, code);
    }

}
