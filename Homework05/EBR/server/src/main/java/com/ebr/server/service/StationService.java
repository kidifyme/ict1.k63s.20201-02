package com.ebr.server.service;

import com.ebr.server.bean.Station;
import com.ebr.server.db.IStationDatabase;
import com.ebr.server.db.JsonStationDatabase;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;

@Path("/stations")
public class StationService {

    private final IStationDatabase stationDatabase;

    public StationService() {
        stationDatabase = JsonStationDatabase.singleton();
    }

    @GET
    @Path("")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Station> getStations(@QueryParam("id") String id, @QueryParam("stationName") String stationName,
                                          @QueryParam("stationAddress") String stationAddress, @QueryParam("contain") String contain,
                                          @QueryParam("hasEmptyDocks") String hasEmptyDocks) {
        Station station = new Station(id, stationName, stationAddress);
        ArrayList<Station> stations = stationDatabase.searchStation(station);
        ArrayList<Station> containRes = new ArrayList<>();
        ArrayList<Station> finalRes = new ArrayList<>();

        if (contain != null) {
            String[] containArray = contain.split(",");
            for (Station s : stations)
                if (s.checkContainedBikeType(containArray))
                    containRes.add(s);
        } else containRes = stations;

        if(hasEmptyDocks != null){
            if (hasEmptyDocks.equals("no")) {
                for (Station s : containRes)
                    if (!s.hasEmptyDocks() && !finalRes.contains(s))
                        finalRes.add(s);
            }

            else {
                for (Station s : containRes)
                    if (s.hasEmptyDocks() && !finalRes.contains(s))
                        finalRes.add(s);
            }
        }
        else finalRes = containRes;
        return finalRes;
    }

    @PUT
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Station updateStation(@PathParam("id") String id, Station station) {
        return stationDatabase.updateStation(id, station);
    }

    @POST
    @Path("")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Station addStation(Station station) {
        return stationDatabase.addStation(station);
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public boolean deleteStation(@PathParam("id") String id) {
        return stationDatabase.deleteStation(id);
    }
}