package com.ebr.server.bean;

public class CalculateCost {
    // tham so o day la tam thoi, thay doi sau
    public double getDepositAmount(Bike bike){
        if(bike instanceof SingleBike) return 400000;
        else if(bike instanceof TwinBike) return 550000;
        else return 700000;
    }
    public double getCost(Bike bike, double rentingTime){
        if(bike instanceof SingleBike){
            if(rentingTime <= 30) return 10000;
            return (10000 + rentingTime/15 * 3000);
        }
        else {
            if(rentingTime <= 30) return 15000;
            return (15000 + rentingTime/15 * 4500);
        }
    }
}
