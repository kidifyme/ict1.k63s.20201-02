package com.ebr.server.db;

import com.ebr.server.bean.Bike;
import com.ebr.server.bean.Station;
import com.ebr.server.db.seed.Seed;

import java.util.ArrayList;

public class JsonStationDatabase implements IStationDatabase{
    private static IStationDatabase singleton = new JsonStationDatabase();

    private ArrayList<Station> stations = Seed.singleton().getStations();
    private ArrayList<Bike> bikes = Seed.singleton().getBikes();

    private JsonStationDatabase() {
    }

    public static IStationDatabase singleton() {
        return singleton;
    }

    @Override
    public ArrayList<Station> searchStation(Station station) {
        ArrayList<Station> res = new ArrayList<Station>();
        for (Station b: stations) {
            if (b.match(station)) {
                res.add(b);
            }
        }
        return res;
    }

    @Override
    public Station addStation(Station station) {
        for (Station m: stations) {
            if (m.equals(station)) {
                return null;
            }
        }

        stations.add(station);
        return station;
    }

    @Override
    public Station updateStation(String id, Station station) {
        for (Station m: stations) {
            if (m.getId().equals(id)) {
                stations.remove(m);
                stations.add(station);
                return station;
            }
        }
        return null;
    }

    @Override
    public boolean deleteStation(String id) {
        for (Station m: stations)
            if (m.getId().equals(id)) {
                stations.remove(m);
                for(Bike bike: bikes){
                    if(bike.getStationId().equals(id)) bike.setStationId("none");
                }
                return true;
            }

        return false;
    }
}
