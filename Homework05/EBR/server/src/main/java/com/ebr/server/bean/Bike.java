package com.ebr.server.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.Date;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({ @JsonSubTypes.Type(value = SingleBike.class, name = "singlebike"), @JsonSubTypes.Type(value = TwinBike.class, name = "twinbike"), @JsonSubTypes.Type(value = EBike.class, name = "elecbike")})
public class Bike {
    private String name;
    private double weight;
    private String licensePlate;
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date manufactureDate;
    private String manufacturer;
    private double cost;
    private String stationId;
    private String code;

    public Bike(String code) {
        this.code = code;
    }

    public Bike() {
        super();
    }

    public Bike(String name, double weight, String licensePlate, Date manufactureDate, String manufacturer, double cost, String stationId, String code) {
        this.name = name;
        this.weight = weight;
        this.licensePlate = licensePlate;
        this.manufactureDate = manufactureDate;
        this.manufacturer = manufacturer;
        this.cost = cost;
        this.stationId = stationId;
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public Date getManufactureDate() {
        return manufactureDate;
    }

    public void setManufactureDate(Date manufactureDate) {
        this.manufactureDate = manufactureDate;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }



    public Bike(String name, String manufacturer, String stationId, String code, String licensePlate) {
        this.name = name;
        this.manufacturer = manufacturer;
        this.stationId = stationId;
        this.code = code;
        this.licensePlate = licensePlate;
    }

    public boolean match(Bike bike) {
        if (bike == null)
            return true;


        if (bike.name != null && !bike.name.equals("") && !this.name.contains(bike.name)) {
            return false;
        }
        if (bike.manufacturer != null && !bike.manufacturer.equals("") && !this.manufacturer.contains(bike.manufacturer)) {
            return false;
        }
        if (bike.stationId != null && !bike.stationId.equals("") && !this.stationId.contains(bike.stationId)) {
            return false;
        }
        if (bike.code != null && !bike.code.equals("") && !this.code.contains(bike.code)) {
            return false;
        }
        if (bike.licensePlate != null && !bike.licensePlate.equals("") && !this.licensePlate.contains(bike.licensePlate)) {
            return false;
        }
        return true;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Bike) {
            return this.code.equals(((Bike) obj).code);
        }
        return false;
    }
}