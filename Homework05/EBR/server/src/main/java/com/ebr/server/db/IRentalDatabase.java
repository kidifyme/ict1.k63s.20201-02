package com.ebr.server.db;

import com.ebr.server.bean.*;

public interface IRentalDatabase {
    public GetRentalResponse getRental(Rental rental);
    public PostRentalRentResponse addRental(RentalRentRequest rentalRentRequest);
    public PostRentalReturnResponse returnBike(RentalReturnRequest rentalReturnRequest);
}
