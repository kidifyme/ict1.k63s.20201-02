package com.ebr.server.service;

import com.ebr.server.bean.*;
import com.ebr.server.db.IRentalDatabase;
import com.ebr.server.db.JsonRentalDatabase;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("")
public class RentalService {
    private final IRentalDatabase rentalDatabase;

    public RentalService() {
        rentalDatabase = JsonRentalDatabase.singleton();
    }

    @GET
    @Path("/rental")
    @Produces(MediaType.APPLICATION_JSON)
    public GetRentalResponse getRental(@QueryParam("cardNumber") String cardNumber){
        if(cardNumber!=null){
            Rental rental = new Rental(cardNumber);
            return rentalDatabase.getRental(rental);
        }
        return null;
    }

    @POST
    @Path("/rental-rent")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public PostRentalRentResponse addRental(RentalRentRequest rentalRentRequest) {
        return rentalDatabase.addRental(rentalRentRequest);
    }

    @POST
    @Path("/rental-return")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public PostRentalReturnResponse returnBike(RentalReturnRequest rentalReturnRequest) {
        return rentalDatabase.returnBike(rentalReturnRequest);
    }

}
