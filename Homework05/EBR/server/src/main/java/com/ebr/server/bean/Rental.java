package com.ebr.server.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonTypeName;

import java.util.Date;

@JsonTypeName("rental")
public class Rental {
    private String bikeCode;
    private String cardNumber;
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date startTime;
    private int rentalId;

    public Rental() {
    }

    public Rental(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getBikeCode() {
        return bikeCode;
    }

    public void setBikeCode(String bikeCode) {
        this.bikeCode = bikeCode;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public int getRentalId() {
        return rentalId;
    }

    public void setRentalId(int rentalId) {
        this.rentalId = rentalId;
    }

    public boolean match(Rental rental) {

        return (this.getCardNumber().equals(rental.getCardNumber()));

    }
}
