package com.ebr.client.components.bike.event.eventargs;

import com.ebr.client.components.base.event.eventargs.EventArgs;

public class RentActionEventArgs extends EventArgs {
    public enum RentAction {
        Rent, Return
    }

    private RentAction rentAction;
    private String bikeCode;

    public RentActionEventArgs(RentAction rentAction) {
        this.rentAction = rentAction;
    }

    public RentActionEventArgs(RentAction rentAction, String bikeCode) {
        this.rentAction = rentAction;
        this.bikeCode = bikeCode;
    }

    public RentAction getRentAction() {
        return rentAction;
    }

    public void setRentAction(RentAction rentAction) {
        this.rentAction = rentAction;
    }

    public String getBikeCode() {
        return bikeCode;
    }

    public void setBikeCode(String bikeCode) {
        this.bikeCode = bikeCode;
    }
}
