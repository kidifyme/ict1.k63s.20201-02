package com.ebr.client.components.base.view;

import com.ebr.client.components.base.event.eventargs.EventArgs;

import java.awt.*;

public class NavigablePane extends EventSourcePanel<EventArgs> {
    private final CardLayout cardLayout;

    public NavigablePane() {
        cardLayout = new CardLayout();
        setLayout(cardLayout);
    }

    public void addComponent(Component component, String componentName) {
        add(component, componentName);
    }

    public void removeComponent(Component component) {
        remove(component);
    }

    public void showComponent(String componentName) {
        cardLayout.show(this, componentName);
    }
}
