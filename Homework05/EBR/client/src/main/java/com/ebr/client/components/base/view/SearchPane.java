package com.ebr.client.components.base.view;

import com.ebr.client.components.base.event.eventargs.SearchEventArgs;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

public abstract class SearchPane extends EventSourcePanel<SearchEventArgs> {
    protected GridBagLayout layout;
    protected GridBagConstraints gbc;

    public SearchPane() {
        layout = new GridBagLayout();
        this.setLayout(layout);

        gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.weightx = 1;
        gbc.gridy = 0;

        buildControls();

        int row = getLastRowIndex();
        gbc.gridx = 0;
        gbc.gridy = row;

        //Search form button
        JButton searchButton = new JButton();
        searchButton.setText("Search");
        searchButton.addActionListener((e) -> fireSearchEvent());

        add(searchButton, gbc);
    }

    protected int getLastRowIndex() {
        layout.layoutContainer(this);
        int[][] dim = layout.getLayoutDimensions();
        int rows = dim[1].length;
        return rows;
    }

    protected abstract void buildControls();

    public void fireSearchEvent() {
        emitEvent(this, new SearchEventArgs(getQueryParams()));
    }

    public Map<String, String> getQueryParams() {
        return new HashMap<String, String>();
    }

    @Override
    public Dimension getMaximumSize() {
        return super.getPreferredSize();
    }
}
