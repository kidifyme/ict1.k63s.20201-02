package com.ebr.client.components.station.view;

import com.ebr.client.components.base.view.SearchPane;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Map;

public class StationSearchPane extends SearchPane {
    protected JTextField stationIdField;
    protected JTextField stationNameField;
    protected JTextField addressField;
    protected JCheckBox hasEmptyDockPointField;
    protected JCheckBox containSingleBikeField;
    protected JCheckBox containTwinBikeField;
    protected JCheckBox containElectricalBikeField;

    @Override
    public void buildControls() {
        //Station name input field
        JLabel stationIdLabel = new JLabel("Station ID: ");
        stationIdField = new JTextField();

        //Station name input field
        JLabel stationNameLabel = new JLabel("Name: ");
        stationNameField = new JTextField();

        //Address input field
        JLabel addressLabel = new JLabel("Address: ");
        addressField = new JTextField();

        //Number of empty dock point input field
        JLabel hasEmptyDockPointLabel = new JLabel("Has empty dock point: ");
        hasEmptyDockPointField = new JCheckBox();

        //Number of available bikes input field
        JLabel hasAvailableBikeLabel = new JLabel("Has available bikes: ");

        // Contain bikes panel
        JPanel containPanel = new JPanel();
        containSingleBikeField = new JCheckBox("Single Bike");
        containTwinBikeField = new JCheckBox("Twin Bike");
        containElectricalBikeField = new JCheckBox("Electrical Bike");

        containPanel.add(containSingleBikeField);
        containPanel.add(containTwinBikeField);
        containPanel.add(containElectricalBikeField);

        //Add area label to panel
        addComponentsToPanel(stationIdLabel, stationIdField);

        //Add station name to panel
        addComponentsToPanel(stationNameLabel, stationNameField);

        //Add address to panel
        addComponentsToPanel(addressLabel, addressField);

        //Add check box of empty dock point to panel
        addComponentsToPanel(hasEmptyDockPointLabel, hasEmptyDockPointField);

        //Add number of available bikes to panel
        addComponentsToPanel(hasAvailableBikeLabel, containPanel);
    }

    protected void addComponentsToPanel(JLabel label, JPanel panel) {
        gbc.gridx = 0;
        gbc.gridy++;
        add(label, gbc);

        gbc.gridx++;
        add(panel, gbc);
    }

    protected void addComponentsToPanel(JLabel label, JCheckBox box) {
        gbc.gridx = 0;
        gbc.gridy++;
        add(label, gbc);

        gbc.gridx++;
        add(box, gbc);
    }

    protected void addComponentsToPanel(JLabel label, JTextField field) {
        gbc.gridx = 0;
        gbc.gridy++;
        add(label, gbc);

        gbc.gridx++;
        add(field, gbc);
    }

    //Set Empty Dock Point Selected
    public void setEmptyDockField() {
        hasEmptyDockPointField.setSelected(true);
    }

    //Set Available Bikes Selected
    public void setAvailableBikesField() {
        containSingleBikeField.setSelected(true);
        containTwinBikeField.setSelected(true);
        containElectricalBikeField.setSelected(true);
    }

    @Override
    public Map<String, String> getQueryParams() {
        Map<String, String> params = super.getQueryParams();

        if (!stationIdField.getText().trim().equals("")) {
            params.put("id", stationIdField.getText().trim());
        }

        if (!stationNameField.getText().trim().equals("")) {
            params.put("stationName", stationNameField.getText().trim());
        }

        if (!addressField.getText().trim().equals("")) {
            params.put("stationAddress", addressField.getText().trim());
        }

        if (hasEmptyDockPointField.isSelected()) {
            params.put("hasEmptyDocks", "yes");
        }

        ArrayList<String> contain = new ArrayList<>(3);

        if (containSingleBikeField.isSelected()) {
            contain.add("singlebike");
        }

        if (containTwinBikeField.isSelected()) {
            contain.add("twinbike");
        }

        if (containElectricalBikeField.isSelected()) {
            contain.add("ebike");
        }

        if (!contain.isEmpty()) {
            params.put("contain", String.join(",", contain));
        }

        return params;
    }

    @Override
    public Dimension getMaximumSize() {
        return getPreferredSize();
    }
}
