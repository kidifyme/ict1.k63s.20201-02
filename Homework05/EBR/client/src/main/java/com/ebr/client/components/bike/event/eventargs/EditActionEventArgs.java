package com.ebr.client.components.bike.event.eventargs;

import com.ebr.client.bean.bike.Bike;
import com.ebr.client.components.base.event.eventargs.EventArgs;

public class EditActionEventArgs extends EventArgs {
    private Bike bike;

    public EditActionEventArgs(Bike bike) {
        this.bike = bike;
    }

    public Bike getBike() {
        return bike;
    }

    public void setBike(Bike bike) {
        this.bike = bike;
    }
}
