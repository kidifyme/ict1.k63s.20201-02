package com.ebr.client.components.base.controller;

import com.ebr.client.components.base.datasource.IDataSource;
import com.ebr.client.components.base.event.eventargs.DataActionEventArgs;
import com.ebr.client.components.base.view.ListPane;
import com.ebr.client.components.base.view.PagePane;
import com.ebr.client.components.base.view.SearchPane;

import javax.swing.*;
import java.util.Map;

/**
 * Controller for {@link com.ebr.client.components.base.view.PagePane}.
 *
 * @param <T> a bean class type
 */
public abstract class PageController<T> {
    protected SearchPane searchPane;
    protected ListPane<T> listPane;
    protected PagePane<T> pagePane;

    public PageController() {
        onLinkingComponents();
    }

    public JPanel getPagePane() {
        return pagePane;
    }

    /**
     * Creates page's components. Subclasses override this method to provide custom components creation.
     */
    protected void onLinkingComponents() {
        IDataSource<T> dataSource = onCreateDataSource();

        // Search pane
        searchPane = onCreateSearchPane();
        searchPane.addEventHandler((sender, eventArgs) -> onSearchEvent(eventArgs.getSearchParams()));

        // List pane
        listPane = onCreateListPane(dataSource);
        listPane.addEventHandler((sender, eventArgs) -> onDataActionEvent(eventArgs));

        // Page pane
        pagePane = onCreatePagePane();
        pagePane.buildControls();
    }

    public void fireSearchEvent() {
        searchPane.fireSearchEvent();
    }

    protected abstract IDataSource<T> onCreateDataSource();

    protected abstract SearchPane onCreateSearchPane();

    protected abstract ListPane<T> onCreateListPane(IDataSource<T> dataSource);

    protected PagePane<T> onCreatePagePane() {
        return new PagePane<T>(searchPane, listPane);
    }

    protected abstract void onDataActionEvent(DataActionEventArgs eventArgs);

    protected abstract void onSearchEvent(Map<String, String> searchParams);
}
