package com.ebr.client.components.station.view;

import com.ebr.client.bean.station.Station;
import com.ebr.client.components.base.event.eventargs.DataEditedEventArgs;
import com.ebr.client.components.base.view.EditDialog;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;

public class StationAddDialog extends EditDialog<Station>{
    private JTextField stationId;
    private JTextField stationName;
    private JTextField stationAddress;
    private JTextField numOfSingleBikes;
    private JTextField numOfTwinBikes;
    private JTextField numOfEBikes;
    private JTextField numOfEmptyDocks;

    private JButton saveButton;

    private JTextField errorStationId;
    private JTextField errorStationName;
    private JTextField errorStationAddress;

    private final String[] stationIdList;

    public StationAddDialog(String[] stationIdList){
        super(null);

        this.setTitle("Add ");
        if (stationIdList == null) {
            stationIdList = new String[0];
        }

        this.stationIdList = stationIdList;
    }

    @Override
    protected void buildSaveButton() {
        saveButton = new JButton("Save");
        saveButton.addActionListener(e -> emitEvent(this, new DataEditedEventArgs<>(getNewData())));
        saveButton.addActionListener(actionEvent -> {
            StationAddDialog.this.dispose();
        });

        c.gridx = 1;
        c.gridy = getLastRowIndex();
        getContentPane().add(saveButton, c);
    }

    @Override
    protected Station getNewData() {
        Station station = new Station(stationId.getText(), stationName.getText(), stationAddress.getText());
        station.setNumOfSingleBikes(Integer.parseInt(numOfSingleBikes.getText()));
        station.setNumOfTwinBikes(Integer.parseInt(numOfTwinBikes.getText()));
        station.setNumOfEBikes(Integer.parseInt(numOfEBikes.getText()));

        return station;
    }

    @Override
    protected void buildControls() {
        //Initialize gbc for add to panel
        c.fill = GridBagConstraints.HORIZONTAL;

        //Add station id
        JLabel stationIdLabel = new JLabel("Station ID: ");
        stationId = new JTextField(15);
        errorStationId = new JTextField(5);
        errorStationId.setForeground(Color.RED);
        addComponentsToPanel(stationIdLabel, stationId, errorStationId);
        validateStationId(stationId, errorStationId);

        //Add station name
        JLabel stationNameLabel = new JLabel("Name: ");
        stationName = new JTextField(15);
        errorStationName = new JTextField(5);
        errorStationName.setForeground(Color.RED);
        addComponentsToPanel(stationNameLabel, stationName, errorStationName);
        validateNULL(stationName, errorStationName);

        //Add station address
        JLabel stationAddressLabel = new JLabel("Address: ");
        stationAddress = new JTextField(15);
        errorStationAddress = new JTextField(5);
        errorStationAddress.setForeground(Color.RED);
        addComponentsToPanel(stationAddressLabel, stationAddress, errorStationAddress);
        validateNULL(stationAddress, errorStationAddress);

        //Add number of single bikes
        JLabel numOfSingleBikesLabel = new JLabel("Number of single bikes: ");
        numOfSingleBikes = new JTextField(15);
        numOfSingleBikes.setText("0");
        numOfSingleBikes.setEditable(false);
        addComponentsToPanel(numOfSingleBikesLabel, numOfSingleBikes);

        //Add number of twin bikes
        JLabel numOfTwinBikesLabel = new JLabel("Number of twin bikes: ");
        numOfTwinBikes = new JTextField(15);
        numOfTwinBikes.setText("0");
        numOfTwinBikes.setEditable(false);
        addComponentsToPanel(numOfTwinBikesLabel, numOfTwinBikes);

        //Add number of electric bikes
        JLabel numOfEBikesLabel = new JLabel("Number of electric bikes: ");
        numOfEBikes = new JTextField(15);
        numOfEBikes.setText("0");
        numOfEBikes.setEditable(false);
        addComponentsToPanel(numOfEBikesLabel, numOfEBikes);

        //Add number of empty docks
        JLabel numOfEmptyDocksLabel = new JLabel("Number of electric bikes: ");
        numOfEmptyDocks = new JTextField(15);
        numOfEmptyDocks.setText("0");
        numOfEmptyDocks.setEditable(false);
        addComponentsToPanel(numOfEmptyDocksLabel, numOfEmptyDocks);

    }

    private void addComponentsToPanel(JLabel label, JTextField field, JTextField error){
        int row = getLastRowIndex();
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(label, c);

        c.gridx = 1;
        getContentPane().add(field, c);

        c.gridx = 2;
        getContentPane().add(error, c);
    }

    private void addComponentsToPanel(JLabel label, JTextField field){
        int row = getLastRowIndex();
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(label, c);

        c.gridx = 1;
        getContentPane().add(field, c);
    }

    protected void validateStationId(JTextField field, JTextField error) {
        field.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                checkNULL();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                checkNULL();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                checkNULL();
            }

            public void checkNULL() {
                if (!field.getText().equals("")) {
                    if(checkDuplicateStationId()){
                        error.setForeground(Color.green);
                        error.setText("Valid");
                        saveButton.setEnabled(true);
                    }else{
                        error.setForeground(Color.red);
                        error.setText("Exist!");
                        saveButton.setEnabled(false);
                    }
                } else {
                    error.setForeground(Color.red);
                    error.setText("Should not NULL");
                    saveButton.setEnabled(false);
                }
            }

            public boolean checkDuplicateStationId(){
                int i;
                for(i = 0; i < stationIdList.length; i++){
                    if(stationId.getText().equals(stationIdList[i])){
                        return false;
                    }
                }
                return true;
            }
        });
    }

    protected void validateNULL(JTextField field, JTextField error) {
        field.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                checkNULL();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                checkNULL();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                checkNULL();
            }

            public void checkNULL() {
                if (!field.getText().equals("")) {
                    error.setForeground(Color.green);
                    error.setText("Valid");
                    saveButton.setEnabled(true);
                } else {
                    error.setForeground(Color.red);
                    error.setText("Should not NULL");
                    saveButton.setEnabled(false);
                }
            }
        });
    }

}
