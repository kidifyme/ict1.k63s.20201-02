package com.ebr.client.components.base.event.eventargs;

public class DataEditDeleteEventArgs extends DataActionEventArgs {
    private DataActionType dataActionType;

    public DataEditDeleteEventArgs(DataActionType dataActionType, int index) {
        super(index);
        this.dataActionType = dataActionType;
    }

    public enum DataActionType {
        EditAction,
        DeleteAction
    }

    public DataActionType getDataActionType() {
        return dataActionType;
    }

    public void setDataActionType(DataActionType dataActionType) {
        this.dataActionType = dataActionType;
    }
}
