package com.ebr.client.components.entrance.view;

import com.ebr.client.bean.creditcard.CreditCard;
import com.ebr.client.components.base.view.EventSourceDialog;
import com.ebr.client.components.entrance.event.eventargs.CreditCardEnteredEventArgs;
import com.ebr.client.serverapi.IServerApi;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public final class EnterCreditCardDialog extends EventSourceDialog<CreditCardEnteredEventArgs> {
    public static final int WINDOW_WIDTH = 200;
    public static final int WINDOW_HEIGHT = 150;

    private final JPanel contentPane;

    private final JTextField creditCardField;
    private final JLabel creditCardValidationLabel;
    private final IServerApi serverApi;

    public EnterCreditCardDialog(Frame owner, IServerApi serverApi) {
        super(owner, "Eco bike rental", true);

        this.serverApi = serverApi;

        contentPane = new JPanel();
        setContentPane(contentPane);
        contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));

        // Credit card number container
        JPanel creditCardNumberContainer = new JPanel();
        creditCardNumberContainer.setLayout(new BoxLayout(creditCardNumberContainer, BoxLayout.X_AXIS));

        // Credit card field
        creditCardField = new JTextField(15);
        creditCardValidationLabel = new JLabel();
        creditCardValidationLabel.setPreferredSize(new Dimension(60, 5));

        // Assemble credit card container
        creditCardNumberContainer.add(Box.createRigidArea(new Dimension(10, 0)));
        creditCardNumberContainer.add(creditCardField);
        creditCardNumberContainer.add(Box.createRigidArea(new Dimension(10, 0)));
        creditCardNumberContainer.add(creditCardValidationLabel);

        // Confirm button
        JButton confirmButton = new JButton("Confirm");
        confirmButton.addActionListener(e -> onConfirmButtonPress());

        // Enter card label
        JLabel enterCardLabel = new JLabel("Enter credit card number");

        // Assemble content pane
        contentPane.add(Box.createRigidArea(new Dimension(0, 5)));

        enterCardLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        contentPane.add(enterCardLabel);

        contentPane.add(Box.createRigidArea(new Dimension(0, 10)));

        creditCardNumberContainer.setAlignmentX(Component.CENTER_ALIGNMENT);
        contentPane.add(creditCardNumberContainer);

        contentPane.add(Box.createRigidArea(new Dimension(0, 10)));

        confirmButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        contentPane.add(confirmButton);

        contentPane.add(Box.createRigidArea(new Dimension(0, 10)));
    }

    private void onConfirmButtonPress() {
        if (creditCardField.getText().trim().equals("")) {
            return;
        }

        // Validate credit card number
        Map<String, String> queryParams = new HashMap<>();
        queryParams.put("cardNumber", creditCardField.getText());
        ArrayList<CreditCard> creditCards = serverApi.getCreditCards(queryParams);

        if (creditCards.size() != 1) {
            creditCardValidationLabel.setText("Not found!");
            return;
        }

        CreditCard creditCard = creditCards.get(0);
        emitEvent(this, new CreditCardEnteredEventArgs(creditCard.getCardNumber()));

        this.dispose();
    }

    public void showDialog() {
        pack();
        setLocationRelativeTo(null);
        this.setVisible(true);
    }
}
