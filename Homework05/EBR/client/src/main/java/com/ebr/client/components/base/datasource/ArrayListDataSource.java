package com.ebr.client.components.base.datasource;

import com.ebr.client.components.base.event.IEventHandler;
import com.ebr.client.components.base.event.eventargs.DataSourceEventArgs;

import javax.swing.event.EventListenerList;
import java.util.ArrayList;

public class ArrayListDataSource<T> implements IDataSource<T> {
    protected EventListenerList listenerList = new EventListenerList();
    protected ArrayList<T> dataList;

    @Override
    public int getSize() {
        if (dataList != null) {
            return dataList.size();
        }

        return 0;
    }

    @Override
    public T getItem(int index) {
        return dataList.get(index);
    }

    public void setSource(ArrayList<T> source) {
        this.dataList = source;

        emitEvent(this,
                new DataSourceEventArgs(DataSourceEventArgs.DataChangedType.SourceChanged));
    }

    public void insertItem(int index, T item) {
        dataList.add(index, item);

        emitEvent(this,
                new DataSourceEventArgs(DataSourceEventArgs.DataChangedType.DataInserted, index));
    }

    public void deleteItem(int index) {
        dataList.remove(index);

        emitEvent(this,
                new DataSourceEventArgs(DataSourceEventArgs.DataChangedType.DataDeleted, index));
    }

    public void modifyItem(int index) {
        emitEvent(this,
                new DataSourceEventArgs(DataSourceEventArgs.DataChangedType.DataModified, index));
    }

    @Override
    public void addEventHandler(IEventHandler<DataSourceEventArgs> eventHandler) {
        listenerList.add(IEventHandler.class, eventHandler);
    }

    @Override
    public void removeEventHandler(IEventHandler<DataSourceEventArgs> eventHandler) {
        listenerList.remove(IEventHandler.class, eventHandler);
    }

    protected void emitEvent(Object sender, DataSourceEventArgs eventArgs) {
        // Guaranteed to return a non-null array
        final Object[] listeners = listenerList.getListenerList();
        // Process the listeners last to first, notifying
        // those that are interested in this event
        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            if (listeners[i] == IEventHandler.class) {
                ((IEventHandler) listeners[i + 1]).onEvent(sender, eventArgs);
            }
        }
    }
}
