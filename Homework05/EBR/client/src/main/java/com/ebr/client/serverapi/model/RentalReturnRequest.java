package com.ebr.client.serverapi.model;

public class RentalReturnRequest {
    private boolean preview;
    private String stationId;
    private String cardNumber;

    public RentalReturnRequest() {
    }

    public RentalReturnRequest(boolean preview, String stationId, String cardNumber) {
        this.preview = preview;
        this.stationId = stationId;
        this.cardNumber = cardNumber;
    }

    public boolean isPreview() {
        return preview;
    }

    public void setPreview(boolean preview) {
        this.preview = preview;
    }

    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }
}
