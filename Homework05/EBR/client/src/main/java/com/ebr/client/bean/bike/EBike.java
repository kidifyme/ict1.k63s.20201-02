package com.ebr.client.bean.bike;

import java.util.Date;

/**
 * Bean class represents an electric bike.
 */
public class EBike extends Bike {
    private int batteryPercentage;
    private int loadCycles;
    private double timeRemaining;

    public EBike() {
    }

    public EBike(String name, String manufacturer, String stationId, String code) {
        super(name, manufacturer, stationId, code);
    }

    public int getBatteryPercentage() {
        return batteryPercentage;
    }

    public void setBatteryPercentage(int batteryPercentage) {
        this.batteryPercentage = batteryPercentage;
    }

    public int getLoadCycles() {
        return loadCycles;
    }

    public void setLoadCycles(int loadCycles) {
        this.loadCycles = loadCycles;
    }

    public double getTimeRemaining() {
        return timeRemaining;
    }

    public void setTimeRemaining(double timeRemaining) {
        this.timeRemaining = timeRemaining;
    }

    public EBike(String name, double weight, String licensePlate, Date manufactureDate, String manufacturer, double cost, String stationId, String code, int batteryPercentage, int loadCycles, double timeRemaining) {
        super(name, weight, licensePlate, manufactureDate, manufacturer, cost, stationId, code);
        this.batteryPercentage = batteryPercentage;
        this.loadCycles = loadCycles;
        this.timeRemaining = timeRemaining;
    }

    public EBike(String name, String manufacturer, String stationId, String code, int batteryPercentage, int loadCycles, double timeRemaining) {
        super(name, manufacturer, stationId, code);
        this.batteryPercentage = batteryPercentage;
        this.loadCycles = loadCycles;
        this.timeRemaining = timeRemaining;
    }

    @Override
    public boolean match(Bike bike) {
        if (bike == null)
            return true;

        boolean res = super.match(bike);

        if (!res) {
            return false;
        }


        if (!(bike instanceof EBike))
            return false;
        return true;
    }
}
