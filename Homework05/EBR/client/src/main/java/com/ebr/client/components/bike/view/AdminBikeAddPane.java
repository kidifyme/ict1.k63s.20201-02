package com.ebr.client.components.bike.view;

import com.ebr.client.components.base.event.eventargs.EventArgs;
import com.ebr.client.components.base.view.EventSourcePanel;

import javax.swing.*;
import java.awt.*;

public class AdminBikeAddPane extends EventSourcePanel<EventArgs> {

    public AdminBikeAddPane() {
        JButton addButton = new JButton("Add");
        addButton.addActionListener(actionEvent -> {
            emitEvent(this, new EventArgs());
        });

        this.add(addButton);
    }

    @Override
    public Dimension getMaximumSize() {
        return getPreferredSize();
    }
}
