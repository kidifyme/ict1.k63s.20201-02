package com.ebr.client.components.bike.view;

import com.ebr.client.bean.bike.Bike;
import com.ebr.client.bean.bike.EBike;
import com.ebr.client.bean.bike.SingleBike;
import com.ebr.client.bean.bike.TwinBike;
import com.ebr.client.components.base.datasource.IDataSource;
import com.ebr.client.components.base.event.eventargs.DataActionEventArgs;
import com.ebr.client.components.base.view.ItemPane;
import com.ebr.client.components.base.view.ListPane;

import javax.swing.*;
import java.awt.*;

public class UserBikeListPane extends ListPane<Bike> {
    protected static class UserBikeItemContainer extends ItemContainer<Bike> {

        @Override
        protected Component onCreateDataHandlingComponent() {
            JPanel panel = new JPanel();
            panel.setLayout(new FlowLayout(FlowLayout.LEFT));

            JButton rentButton = new JButton("Rent");
            rentButton.addActionListener(actionEvent -> {
                if (eventEmitter != null) {
                    eventEmitter.emitEvent(this, new DataActionEventArgs(getIndex()));
                }
            });

            panel.add(rentButton);

            return panel;
        }
    }

    public UserBikeListPane(IDataSource<Bike> dataSource) {
        super(dataSource);
    }

    @Override
    protected ItemPane<Bike> onCreateItemPane(Bike data) {
        if (data instanceof SingleBike) {
            return new SingleBikeItemPane();
        }
        if (data instanceof EBike) {
            return new EBikeItemPane();
        }
        if (data instanceof TwinBike) {
            return new TwinBikeItemPane();
        }

        return new BikeItemPane();
    }

    @Override
    protected ItemContainer<Bike> onCreateItemContainer() {
        return new UserBikeItemContainer();
    }
}
