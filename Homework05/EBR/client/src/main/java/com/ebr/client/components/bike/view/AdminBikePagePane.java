package com.ebr.client.components.bike.view;

import com.ebr.client.bean.bike.Bike;
import com.ebr.client.components.base.view.ListPane;
import com.ebr.client.components.base.view.PagePane;
import com.ebr.client.components.base.view.SearchPane;

import javax.swing.*;
import java.awt.*;

public class AdminBikePagePane extends PagePane<Bike> {

    protected final AdminBikeAddPane adminBikeAddPane;

    public AdminBikePagePane(SearchPane searchPane, ListPane<Bike> listPane, AdminBikeAddPane adminBikeAddPane) {
        super(searchPane, listPane);
        this.adminBikeAddPane = adminBikeAddPane;
    }

    @Override
    public void buildControls() {
        // Container holding search pane and renting pane
        JPanel topContainer = new JPanel();
        topContainer.setLayout(new BoxLayout(topContainer, BoxLayout.X_AXIS));

        // Search pane
        searchPane.setAlignmentY(Component.TOP_ALIGNMENT);
        topContainer.add(searchPane);
        topContainer.add(Box.createHorizontalGlue());

        adminBikeAddPane.setAlignmentY(Component.TOP_ALIGNMENT);
        topContainer.add(adminBikeAddPane);

        layout.putConstraint(SpringLayout.WEST, topContainer, 5, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, topContainer, 5, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.EAST, topContainer, -5, SpringLayout.EAST, this);
        add(topContainer);

        // List pane
        layout.putConstraint(SpringLayout.WEST, listPane, 5, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, listPane, 5, SpringLayout.SOUTH, topContainer);
        layout.putConstraint(SpringLayout.EAST, listPane, -5, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.SOUTH, listPane, -5, SpringLayout.SOUTH, this);
        add(listPane);
    }
}
