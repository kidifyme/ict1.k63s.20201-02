package com.ebr.client.components.bike.view;

import com.ebr.client.bean.bike.Bike;
import com.ebr.client.bean.bike.EBike;
import com.ebr.client.bean.bike.SingleBike;
import com.ebr.client.bean.bike.TwinBike;
import com.ebr.client.components.base.event.eventargs.EventArgs;
import com.ebr.client.components.base.view.EventSourceDialog;
import com.ebr.client.serverapi.model.PostRentalRentResponse;

import javax.swing.*;
import java.awt.*;

public class BikeRentDialog extends EventSourceDialog<EventArgs> {

    private GridBagLayout layout;
    private GridBagConstraints c = new GridBagConstraints();

    private JTextField bikeCodeField;
    private JTextField bikeTypeField;
    private JTextField bikeLicensePlateField;
    private JTextField statusField;
    private JTextField balanceField;
    private JTextField depositAmountField;

    public BikeRentDialog(Bike bike, PostRentalRentResponse postRentalRentResponse) {
        super(null, "Rent", true);

        setContentPane(new JPanel());
        layout = new GridBagLayout();
        getContentPane().setLayout(layout);

        this.buildControls(bike, postRentalRentResponse);

        JButton confirmButton = new JButton("Confirm");

        // Validate rental request
        if (postRentalRentResponse.getStatus().equals("ok")) {
            confirmButton.addActionListener(e -> {
                emitEvent(this, new EventArgs());
                this.dispose();
            });
        } else {
            confirmButton.setEnabled(false);
        }

        c.gridx = 1;
        c.gridy = getLastRowIndex();
        getContentPane().add(confirmButton, c);

        this.pack();
    }

    private int getLastRowIndex() {
        layout.layoutContainer(getContentPane());
        int[][] dim = layout.getLayoutDimensions();
        int rows = dim[1].length;
        return rows;
    }

    private void buildControls(Bike bike, PostRentalRentResponse postRentalRentResponse) {
        int row = getLastRowIndex();

        JLabel bikeCodeLabel = new JLabel("Bike code: ");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(bikeCodeLabel, c);
        bikeCodeField = new JTextField(15);
        bikeCodeField.setText(bike.getCode());
        bikeCodeField.setEditable(false);
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(bikeCodeField, c);

        row = getLastRowIndex();
        JLabel bikeTypeLabel = new JLabel("Bike type: ");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(bikeTypeLabel, c);
        bikeTypeField = new JTextField(15);
        if (bike instanceof SingleBike) bikeTypeField.setText("Single bike");
        if (bike instanceof TwinBike) bikeTypeField.setText("Twin bike");
        if (bike instanceof EBike) bikeTypeField.setText("Electric bike");
        bikeTypeField.setEditable(false);
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(bikeTypeField, c);

        row = getLastRowIndex();
        JLabel bikeLicensePlateLabel = new JLabel("Bike license plate: ");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(bikeLicensePlateLabel, c);
        bikeLicensePlateField = new JTextField(15);
        bikeLicensePlateField.setText(bike.getLicensePlate());
        bikeLicensePlateField.setEditable(false);
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(bikeLicensePlateField, c);

        row = getLastRowIndex();
        JLabel statusLabel = new JLabel("Status: ");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(statusLabel, c);
        statusField = new JTextField(15);
        statusField.setText(postRentalRentResponse.getStatus());
        statusField.setEditable(false);
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(statusField, c);

        row = getLastRowIndex();
        JLabel balanceLabel = new JLabel("Balance: ");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(balanceLabel, c);
        balanceField = new JTextField(15);
        balanceField.setText(String.valueOf(postRentalRentResponse.getBalance()));
        balanceField.setEditable(false);
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(balanceField, c);

        row = getLastRowIndex();
        JLabel depositAmountLabel = new JLabel("Deposit amount: ");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(depositAmountLabel, c);
        depositAmountField = new JTextField(15);
        depositAmountField.setText(String.valueOf(postRentalRentResponse.getDepositAmount()));
        depositAmountField.setEditable(false);
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(depositAmountField, c);

    }
}
