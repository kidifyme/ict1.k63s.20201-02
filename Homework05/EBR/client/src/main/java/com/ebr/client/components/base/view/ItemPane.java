package com.ebr.client.components.base.view;

import javax.swing.*;
import java.awt.*;

/**
 * Displays an item in {@link com.ebr.client.components.base.view.ListPane}.
 *
 * @param <T> a bean class type
 */
public abstract class ItemPane<T> extends JPanel {
    protected T data;

    protected GridBagLayout layout;
    protected GridBagConstraints reusableConstraints;

    public ItemPane() {
        onBuildControls();
    }

    public void updateData(T data) {
        this.data = data;
        displayData();
    }

    public T getData() {
        return this.data;
    }

    protected void onBuildControls() {
        layout = new GridBagLayout();
        setLayout(layout);
        reusableConstraints = new GridBagConstraints();
        reusableConstraints.fill = GridBagConstraints.HORIZONTAL;
        reusableConstraints.weightx = 1;
    }

    protected abstract void displayData();

    protected int getLastRowIndex() {
        layout.layoutContainer(this);
        int[][] dimension = layout.getLayoutDimensions();
        return dimension[1].length;
    }
}
