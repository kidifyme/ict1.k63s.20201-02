package com.ebr.client.serverapi;

import com.ebr.client.bean.bike.Bike;
import com.ebr.client.bean.creditcard.CreditCard;
import com.ebr.client.bean.station.Station;
import com.ebr.client.serverapi.model.*;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Map;

public interface IServerApi {
    ArrayList<Bike> getBikes(Map<String, String> queryParams);

    Bike updateBike(Bike bike);

    Bike addBike(Bike bike);

    Response deleteBike(Bike bike);

    ArrayList<Station> getStations(Map<String, String> queryParams);

    Station updateStation(Station station);

    Station addStation(Station station);

    Response deleteStation(Station station);

    ArrayList<CreditCard> getCreditCards(Map<String, String> queryParams);

    GetRentalResponse getRental(Map<String, String> queryParams);

    PostRentalRentResponse addRental(RentalRentRequest rentalRentRequest);

    PostRentalReturnResponse returnBike(RentalReturnRequest rentalReturnRequest);
}
