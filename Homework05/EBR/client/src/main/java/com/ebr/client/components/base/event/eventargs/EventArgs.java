package com.ebr.client.components.base.event.eventargs;

/**
 * Subclasses that want to provide event information can extend this base.
 *
 * @see com.ebr.client.components.base.event.IEventSource
 */
public class EventArgs {
}
