package com.ebr.client.components.base.event;

import com.ebr.client.components.base.event.eventargs.EventArgs;

/**
 * Enforces event emitting capability. This interface complements
 * {@link com.ebr.client.components.base.event.IEventSource}.
 *
 * @param <TEventArgs> a custom class that provides event information
 */
public interface IEventEmitter<TEventArgs extends EventArgs> {
    void emitEvent(Object sender, TEventArgs eventArgs);
}
