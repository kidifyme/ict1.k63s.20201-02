package com.ebr.client.components.bike.view;

import com.ebr.client.bean.bike.Bike;
import com.ebr.client.components.base.event.eventargs.DataEditedEventArgs;
import com.ebr.client.components.base.view.EditDialog;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BikeEditDialog extends EditDialog<Bike> {

    private JTextField codeField;
    private JTextField nameField;
    private JTextField weightField;
    private JTextField licenseField;
    private JTextField dateField;
    private JTextField manufacturerField;
    private JTextField costField;

    private JComboBox<String> yearListBox;
    private JComboBox<String> monthListBox;
    private JComboBox<String> dayListBox;

    private JTextField errorCodeField;
    private JTextField errorNameField;
    private JTextField errorWeightField;
    private JTextField errorLicenseField;
    private JTextField errorManufacturerField;
    private JTextField errorCostField;
    private JTextField errorDateField;
    private JTextField errorBatteryField;
    private JTextField errorLoadCyclesField;
    private JTextField errorTimeRemainingField;

    private JButton saveButton;

    public BikeEditDialog(Bike editBike) {
        super(editBike);
        System.out.println(editBike.getCode());
    }

    @Override
    protected void buildSaveButton() {
        saveButton = new JButton("Save");

        saveButton.addActionListener(actionEvent -> {
            emitEvent(this, new DataEditedEventArgs<>(getNewData()));
            this.dispose();
        });

        c.gridx = 1;
        c.gridy = getLastRowIndex();
        getContentPane().add(saveButton, c);
    }

    @Override
    protected void buildControls() {
        int row = getLastRowIndex();

        row = getLastRowIndex();
        JLabel codeLabel = new JLabel("Code: ");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(codeLabel, c);
        codeField = new JTextField(15);
        codeField.setText(t.getCode());
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(codeField, c);
        errorCodeField = new JTextField("", 10);
        errorCodeField.setForeground(Color.RED);
        errorCodeField.setEditable(false);
        c.gridx = 4;
        c.gridy = row;
        getContentPane().add(errorCodeField, c);
        validateNULL(codeField, errorCodeField);

        row = getLastRowIndex();
        JLabel nameLabel = new JLabel("Name: ");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(nameLabel, c);
        nameField = new JTextField(15);
        nameField.setText(t.getName());
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(nameField, c);
        errorNameField = new JTextField("", 10);
        errorNameField.setForeground(Color.RED);
        errorNameField.setEditable(false);
        c.gridx = 4;
        c.gridy = row;
        getContentPane().add(errorNameField, c);
        validateNULL(nameField, errorNameField);

        row = getLastRowIndex();
        JLabel weightLabel = new JLabel("Weight: ");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(weightLabel, c);
        weightField = new JTextField(15);
        weightField.setText(String.valueOf(t.getWeight()));
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(weightField, c);
        errorWeightField = new JTextField("", 10);
        errorWeightField.setForeground(Color.RED);
        errorWeightField.setEditable(false);
        c.gridx = 4;
        c.gridy = row;
        getContentPane().add(errorWeightField, c);
        validateDouble(weightField, errorWeightField);

        row = getLastRowIndex();
        JLabel licenseLabel = new JLabel("License plate: ");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(licenseLabel, c);
        licenseField = new JTextField(15);
        licenseField.setText(t.getLicensePlate());
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(licenseField, c);
        errorLicenseField = new JTextField("", 10);
        errorLicenseField.setForeground(Color.RED);
        errorLicenseField.setEditable(false);
        c.gridx = 4;
        c.gridy = row;
        getContentPane().add(errorLicenseField, c);
        validateNULL(licenseField, errorLicenseField);

        row = getLastRowIndex();
        JLabel dateLabel = new JLabel("Manufacture Date: ");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(dateLabel, c);

        String[] yearString = new String[20];
        for (int i = 0; i < 20; i++) {
            yearString[i] = String.valueOf(2000 + i);
        }
        yearListBox = new JComboBox<>(yearString);
        yearListBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setOutputDate();
            }
        });
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(yearListBox, c);

        String[] monthString = new String[12];
        for (int i = 0; i < 12; i++) {
            monthString[i] = String.valueOf(1 + i);
        }
        monthListBox = new JComboBox<>(monthString);
        monthListBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setOutputDate();
            }
        });
        c.gridx = 2;
        c.gridy = row;
        getContentPane().add(monthListBox, c);

        String[] dayString = new String[31];
        for (int i = 0; i < 31; i++) {
            dayString[i] = String.valueOf(1 + i);
        }
        dayListBox = new JComboBox<>(dayString);
        dayListBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setOutputDate();
            }
        });
        c.gridx = 3;
        c.gridy = row;
        getContentPane().add(dayListBox, c);
        errorDateField = new JTextField("", 10);
        errorDateField.setForeground(Color.ORANGE);
        errorDateField.setEditable(false);
        c.gridx = 4;
        c.gridy = row;
        getContentPane().add(errorDateField, c);

        row = getLastRowIndex();
        JLabel manufacturerLabel = new JLabel("Manufacturer: ");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(manufacturerLabel, c);
        manufacturerField = new JTextField(15);
        manufacturerField.setText(t.getManufacturer());
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(manufacturerField, c);
        errorManufacturerField = new JTextField("", 10);
        errorManufacturerField.setForeground(Color.RED);
        errorManufacturerField.setEditable(false);
        c.gridx = 4;
        c.gridy = row;
        getContentPane().add(errorManufacturerField, c);
        validateNULL(manufacturerField, errorManufacturerField);

        row = getLastRowIndex();
        JLabel costLabel = new JLabel("Cost: ");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(costLabel, c);
        costField = new JTextField(15);
        costField.setText(String.valueOf(t.getCost()));
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(costField, c);
        errorCostField = new JTextField("", 10);
        errorCostField.setForeground(Color.RED);
        errorCostField.setEditable(false);
        c.gridx = 4;
        c.gridy = row;
        getContentPane().add(errorCostField, c);
        validateDouble(costField, errorCostField);
    }

    @Override
    protected Bike getNewData() {
        String dateString;
        t.setCode(codeField.getText());
        t.setName(nameField.getText());
        t.setWeight(Float.parseFloat(weightField.getText()));
        t.setLicensePlate(licenseField.getText());
        dateString = yearListBox.getSelectedItem() + "-" + monthListBox.getSelectedItem() + "-" + dayListBox.getSelectedItem();
        try {
            t.setManufactureDate(new SimpleDateFormat("yyyy-MM-dd").parse(dateString));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        t.setManufacturer(manufacturerField.getText());
        t.setCost(Double.parseDouble(costField.getText()));
        return t;
    }

    protected void validateNULL(JTextField textField, JTextField errorTextField) {
        textField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                checkNULL();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                checkNULL();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                checkNULL();
            }

            public void checkNULL() {
                if (!textField.getText().equals("")) {
                    errorTextField.setForeground(Color.ORANGE);
                    errorTextField.setText("Valid");
                    saveButton.setEnabled(true);
                } else {
                    errorTextField.setForeground(Color.red);
                    errorTextField.setText("Should not NULL");
                    saveButton.setEnabled(false);
                }
            }
        });
    }

    protected void validateDouble(JTextField textField, JTextField errorTextField) {
        textField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                checkDouble();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                checkDouble();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                checkDouble();
            }

            public void checkDouble() {
                try {
                    Double i = Double.parseDouble(textField.getText());
                    errorTextField.setText("Valid");
                    errorTextField.setForeground(Color.ORANGE);
                    saveButton.setEnabled(true);
                } catch (NumberFormatException e1) {
                    errorTextField.setText("Double required");
                    saveButton.setEnabled(false);
                }
            }
        });
    }

    protected void validateInt(JTextField textField, JTextField errorTextField) {
        textField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                checkDouble();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                checkDouble();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                checkDouble();
            }

            public void checkDouble() {
                try {
                    int i = Integer.parseInt(textField.getText());
                    errorTextField.setText("Valid");
                    errorTextField.setForeground(Color.ORANGE);
                    saveButton.setEnabled(true);
                } catch (NumberFormatException e1) {
                    errorTextField.setText("Integer required");
                    saveButton.setEnabled(false);
                }
            }
        });
    }

    private void setOutputDate() {
        String dateString = yearListBox.getSelectedItem() + "-" + monthListBox.getSelectedItem() + "-" + dayListBox.getSelectedItem();
        Date inputDate = null;
        try {
            inputDate = new SimpleDateFormat("yyyy-MM-dd").parse(dateString);
        } catch (ParseException parseException) {
            parseException.printStackTrace();
        }

        SimpleDateFormat dateFormatOutput = new SimpleDateFormat("yyyy-MM-dd");
        String x = dateFormatOutput.format(inputDate);
        errorDateField.setText(x);
    }
}
