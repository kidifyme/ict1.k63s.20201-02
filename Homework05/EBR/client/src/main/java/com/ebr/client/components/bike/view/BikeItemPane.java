package com.ebr.client.components.bike.view;

import com.ebr.client.bean.bike.Bike;
import com.ebr.client.components.base.view.ItemPane;

import javax.swing.*;
import java.text.DecimalFormat;

public class BikeItemPane extends ItemPane<Bike> {
    private JLabel labelCode;
    private JLabel labelName;
    private JLabel labelWeight;
    private JLabel labelLicensePlate;
    private JLabel labelManufactureDate;
    private JLabel labelManufacturer;
    private JLabel labelCost;

    private final DecimalFormat costFormat;

    public BikeItemPane() {
        super();
        costFormat = new DecimalFormat("$###,###.###");
    }

    @Override
    protected void onBuildControls() {
        super.onBuildControls();

        int row = getLastRowIndex();

        // Label code
        reusableConstraints.gridx = 0;
        reusableConstraints.gridy = row;
        labelCode = new JLabel();
        add(labelCode, reusableConstraints);

        // Label name
        row++;
        reusableConstraints.gridx = 0;
        reusableConstraints.gridy = row;
        labelName = new JLabel();
        add(labelName, reusableConstraints);

        // Label weight
        row++;
        reusableConstraints.gridx = 0;
        reusableConstraints.gridy = row;
        labelWeight = new JLabel();
        add(labelWeight, reusableConstraints);

        // Label license plate
        row++;
        reusableConstraints.gridx = 0;
        reusableConstraints.gridy = row;
        labelLicensePlate = new JLabel();
        add(labelLicensePlate, reusableConstraints);

        // Label manufacture date
        row++;
        reusableConstraints.gridx = 0;
        reusableConstraints.gridy = row;
        labelManufactureDate = new JLabel();
        add(labelManufactureDate, reusableConstraints);

        // Label manufacturer
        row++;
        reusableConstraints.gridx = 0;
        reusableConstraints.gridy = row;
        labelManufacturer = new JLabel();
        add(labelManufacturer, reusableConstraints);

        // Label cost
        row++;
        reusableConstraints.gridx = 0;
        reusableConstraints.gridy = row;
        labelCost = new JLabel();
        add(labelCost, reusableConstraints);
    }

    @Override
    protected void displayData() {
        labelCode.setText(String.format("Code: %s", data.getCode()));
        labelName.setText(String.format("Name: %s", data.getName()));
        labelWeight.setText(String.format("Weight: %.2f kg", data.getWeight()));
        labelLicensePlate.setText(String.format("License plate: %s", data.getLicensePlate()));
        labelManufactureDate.setText(String.format("Manufacture date: %s", data.getManufactureDate().toString()));
        labelManufacturer.setText(String.format("Manufacturer: %s", data.getManufacturer()));

        labelCost.setText(String.format("Cost: %s", costFormat.format(data.getCost())));
    }
}
