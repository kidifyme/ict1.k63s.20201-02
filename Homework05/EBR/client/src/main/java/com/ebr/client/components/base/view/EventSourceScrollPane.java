package com.ebr.client.components.base.view;

import com.ebr.client.components.base.event.IEventHandler;
import com.ebr.client.components.base.event.IEventSource;
import com.ebr.client.components.base.event.eventargs.EventArgs;

import javax.swing.*;

/**
 * A convenient class that extends from <b>JScrollPane</b> and
 * implements <b>IEventSource</b>.
 * <p/>
 * Subclasses can emit events by invoking <b>emitEvent</b>.
 *
 * @param <TEventArgs> a custom class that provides event information
 */

public class EventSourceScrollPane<TEventArgs extends EventArgs>
        extends JScrollPane
        implements IEventSource<TEventArgs> {

    @Override
    public void addEventHandler(IEventHandler<TEventArgs> eventHandler) {
        listenerList.add(IEventHandler.class, eventHandler);
    }

    @Override
    public void removeEventHandler(IEventHandler<TEventArgs> eventHandler) {
        listenerList.remove(IEventHandler.class, eventHandler);
    }

    protected void emitEvent(Object sender, TEventArgs eventArgs) {
        // Guaranteed to return a non-null array
        final Object[] listeners = listenerList.getListenerList();
        // Process the listeners last to first, notifying
        // those that are interested in this event
        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            if (listeners[i] == IEventHandler.class) {
                ((IEventHandler) listeners[i + 1]).onEvent(sender, eventArgs);
            }
        }
    }
}
