package com.ebr.client.components.bike.view;

import com.ebr.client.components.base.view.EventSourcePanel;
import com.ebr.client.components.bike.event.eventargs.RentActionEventArgs;

import javax.swing.*;
import java.awt.*;

public final class BikeRentingPane extends EventSourcePanel<RentActionEventArgs> {

    private GridBagLayout layout;
    private GridBagConstraints reusableConstraints;

    private JTextField bikeCodeField;
    JButton rentButton;
    JButton returnButton;

    public BikeRentingPane() {
        buildControls();
    }

    public void setBikeCode(String bikeCode) {
        bikeCodeField.setText(bikeCode);
    }

    public void clearBikeCode() {
        bikeCodeField.setText(null);
    }

    public void setRentalStatus(boolean renting) {
        if (renting) {
            rentButton.setEnabled(false);
            returnButton.setEnabled(true);
        } else {
            rentButton.setEnabled(true);
            returnButton.setEnabled(false);
        }
    }

    public String getBikeCode() {
        return bikeCodeField.getText();
    }

    private void buildControls() {
        layout = new GridBagLayout();
        setLayout(layout);
        reusableConstraints = new GridBagConstraints();
        reusableConstraints.fill = GridBagConstraints.HORIZONTAL;
        reusableConstraints.weightx = 1;

        int row = 0;

        // Bike code label
        JLabel bikeCodeLabel = new JLabel("Bike code: ");
        reusableConstraints.gridx = 0;
        reusableConstraints.gridy = row;
        add(bikeCodeLabel, reusableConstraints);

        // Bike code field
        bikeCodeField = new JTextField(5);
        reusableConstraints.gridx = 1;
        reusableConstraints.gridy = row;
        add(bikeCodeField, reusableConstraints);

        // Container for rent and return buttons
        JPanel buttonGroup = new JPanel(new FlowLayout(FlowLayout.RIGHT));

        // Rent button
        rentButton = new JButton("Rent");
        rentButton.addActionListener((e) -> {
            emitEvent(this, new RentActionEventArgs(RentActionEventArgs.RentAction.Rent, getBikeCode()));
        });
        buttonGroup.add(rentButton);

        // Return button
        returnButton = new JButton("Return");
        returnButton.addActionListener((e) -> {
            emitEvent(this, new RentActionEventArgs(RentActionEventArgs.RentAction.Return));
        });
        buttonGroup.add(returnButton);

        row++;
        reusableConstraints.gridx = 1;
        reusableConstraints.gridy = row;
        add(buttonGroup, reusableConstraints);
    }

    @Override
    public Dimension getMaximumSize() {
        return getPreferredSize();
    }
}
