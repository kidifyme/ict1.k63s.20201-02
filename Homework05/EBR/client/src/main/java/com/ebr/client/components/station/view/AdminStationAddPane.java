package com.ebr.client.components.station.view;

import com.ebr.client.components.base.event.eventargs.EventArgs;
import com.ebr.client.components.base.view.EventSourcePanel;

import javax.swing.*;
import java.awt.*;

public class AdminStationAddPane extends EventSourcePanel<EventArgs> {
    public AdminStationAddPane() {
        JButton addButton = new JButton("Add");
        addButton.addActionListener(actionEvent -> {
            emitEvent(this, new EventArgs());
        });

        this.add(addButton);
    }

    @Override
    public Dimension getMaximumSize() {
        return getPreferredSize();
    }
}
