package com.ebr.client.components.bike.view;

public class UserBikeSearchPane extends BikeSearchPane {

    @Override
    public void buildControls() {
        super.buildControls();

        stationID.setEditable(false);
    }

    public void setContainingStationId(String containingStationId) {
        stationID.setText(containingStationId);
    }
}
