package com.ebr.client.app.user;

import com.ebr.client.components.base.event.eventargs.NavigationControllerEventArgs;
import com.ebr.client.components.base.view.NavigablePane;
import com.ebr.client.components.base.view.NavigationControllerPane;
import com.ebr.client.components.bike.controller.UserBikePageController;
import com.ebr.client.components.station.controller.UserStationPageController;
import com.ebr.client.components.status.view.StatusPane;
import com.ebr.client.serverapi.IServerApi;
import com.ebr.client.serverapi.ServerAPI;
import com.ebr.client.serverapi.model.GetRentalResponse;

import javax.swing.*;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

public class EBRUserController {
    private static final String STATION_PAGE = "Station";
    private static final String BIKE_PAGE = "Bike";

    private final JPanel rootPanel;

    private final IServerApi serverApi;

    private final NavigationControllerPane navigationControllerPane;
    private final NavigablePane navigablePane;
    private final StatusPane statusPane;

    private final UserStationPageController stationPageController;
    private final UserBikePageController bikePageController;

    private boolean inStation;

    public EBRUserController() {
        rootPanel = new JPanel();
        BorderLayout layout = new BorderLayout();
        rootPanel.setLayout(layout);

        // Server api
        serverApi = new ServerAPI();

        // Navigation controller pane
        navigationControllerPane = new NavigationControllerPane();
        navigationControllerPane.addEventHandler((sender, eventArgs) -> onNavigationEvent(eventArgs));

        // Status pane
        statusPane = new StatusPane();
        statusPane.addEventHandler((sender, eventArgs) -> onRequestStatusUpdateEvent());

        // Station page controller
        stationPageController = new UserStationPageController(serverApi);
        stationPageController.setStationSelectedEventHandler((sender, eventArgs) -> showBikePage(eventArgs.getStationId()));

        // Bike page controller
        bikePageController = new UserBikePageController(serverApi, (sender, eventArgs) -> onBikeRentActionProcessed());

        // Navigable pane
        navigablePane = new NavigablePane();
        navigablePane.addComponent(stationPageController.getPagePane(), STATION_PAGE);
        navigablePane.addComponent(bikePageController.getPagePane(), BIKE_PAGE);

        // Top container (navigation controller pane + status pane)
        JPanel topContainer = new JPanel();
        topContainer.setBorder(new CompoundBorder(new MatteBorder(0, 0, 1, 0, Color.BLACK), new EmptyBorder(5, 5, 5, 5)));
        topContainer.setLayout(new BoxLayout(topContainer, BoxLayout.X_AXIS));

        navigationControllerPane.setAlignmentY(Component.TOP_ALIGNMENT);
        topContainer.add(navigationControllerPane);
        topContainer.add(Box.createHorizontalGlue());

        statusPane.setAlignmentY(Component.TOP_ALIGNMENT);
        topContainer.add(statusPane);

        // Assemble root panel
        rootPanel.add(topContainer, BorderLayout.NORTH);
        rootPanel.add(navigablePane, BorderLayout.CENTER);

        // Kick things off
        statusPane.fireRequestUpdateEvent();
        stationPageController.fireSearchEvent();

        // Show station page at start
        showStationPage();
        navigationControllerPane.setNavigateForwardEnabled(false);
        navigationControllerPane.setNavigateBackwardEnabled(false);
        inStation = true;
    }

    public JPanel getRootPanel() {
        return rootPanel;
    }

    private void onNavigationEvent(NavigationControllerEventArgs eventArgs) {
        switch (eventArgs.getType()) {
            case Backward:
                showStationPage();
                break;
            case Forward:
                showBikePage(null);
                break;
        }
    }

    private void showStationPage() {
        navigablePane.showComponent(STATION_PAGE);
        navigationControllerPane.setNavigateForwardEnabled(true);
        navigationControllerPane.setNavigateBackwardEnabled(false);
        inStation = true;
    }

    private void showBikePage(String stationId) {
        if (stationId != null) {
            bikePageController.setContainingStationId(stationId);
            bikePageController.clearChosenBikeCode();
            bikePageController.fireSearchEvent();
        }

        navigablePane.showComponent(BIKE_PAGE);
        navigationControllerPane.setNavigateBackwardEnabled(true);
        navigationControllerPane.setNavigateForwardEnabled(false);
        inStation = false;
    }

    private void onRequestStatusUpdateEvent() {
        // Get bike rental info
        Map<String, String> rentalRequestParams = new HashMap<>();
        rentalRequestParams.put("cardNumber", UserInfo.getCreditCardNumber());
        GetRentalResponse rentalResponse = serverApi.getRental(rentalRequestParams);

        if (rentalResponse != null) {
            statusPane.updateStatus(rentalResponse.getBikeType(), rentalResponse.getBikeCode(), rentalResponse.getRentingTime());
            bikePageController.setRentalStatus(true);
        } else {
            statusPane.updateStatus();
            bikePageController.setRentalStatus(false);
        }
    }

    private void onBikeRentActionProcessed() {
        // Update status pane
        statusPane.fireRequestUpdateEvent();

        // Update station page
        stationPageController.fireSearchEvent();

        // Update bike page
        bikePageController.fireSearchEvent();
    }
}
