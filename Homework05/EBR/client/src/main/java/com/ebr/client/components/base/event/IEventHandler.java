package com.ebr.client.components.base.event;

import com.ebr.client.components.base.event.eventargs.EventArgs;

import java.util.EventListener;

/**
 * An interface for receiving events. When an event occurs, <b>onEvent</b> method
 * is invoked.
 *
 * @param <TEventArgs> a custom class that provides event information
 * @see com.ebr.client.components.base.event.IEventSource
 */
public interface IEventHandler<TEventArgs extends EventArgs> extends EventListener {
    void onEvent(Object sender, TEventArgs eventArgs);
}
