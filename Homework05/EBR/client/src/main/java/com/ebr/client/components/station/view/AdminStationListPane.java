package com.ebr.client.components.station.view;

import com.ebr.client.bean.station.Station;
import com.ebr.client.components.base.datasource.IDataSource;
import com.ebr.client.components.base.event.eventargs.DataActionEventArgs;
import com.ebr.client.components.base.event.eventargs.DataEditDeleteEventArgs;
import com.ebr.client.components.base.view.ItemPane;
import com.ebr.client.components.base.view.ListPane;

import javax.swing.*;
import java.awt.*;

public class AdminStationListPane extends ListPane<Station> {
    protected static class AdminStationItemContainer extends ItemContainer<Station> {
        @Override
        protected Component onCreateDataHandlingComponent() {

            JPanel panel = new JPanel();
            panel.setLayout(new FlowLayout(FlowLayout.LEFT));

            JButton editButton = new JButton("Edit");
            editButton.addActionListener(actionEvent -> {
                if (eventEmitter != null) {
                    eventEmitter.emitEvent(this, new DataEditDeleteEventArgs(DataEditDeleteEventArgs.DataActionType.EditAction, getIndex()));
                }
            });
            panel.add(editButton);

            JButton deleteButton = new JButton("Delete");
            deleteButton.addActionListener(actionEvent -> {
                if (eventEmitter != null) {
                    eventEmitter.emitEvent(this, new DataEditDeleteEventArgs(DataEditDeleteEventArgs.DataActionType.DeleteAction, getIndex()));
                }
            });
            panel.add(deleteButton);

            return panel;
        }
    }

    public AdminStationListPane(IDataSource<Station> dataSource) {
        super(dataSource);
    }

    @Override
    protected ItemPane<Station> onCreateItemPane(Station station) {
        return new StationItemPane();
    }

    @Override
    protected ItemContainer<Station> onCreateItemContainer() {
        return new AdminStationItemContainer();
    }
}
