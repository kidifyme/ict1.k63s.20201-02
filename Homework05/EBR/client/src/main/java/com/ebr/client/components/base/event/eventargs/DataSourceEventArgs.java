package com.ebr.client.components.base.event.eventargs;

public class DataSourceEventArgs extends EventArgs {
    public enum DataChangedType {
        SourceChanged,
        DataInserted,
        DataDeleted,
        DataModified
    }

    private DataChangedType type;
    private int index;

    public DataSourceEventArgs(DataChangedType type) {
        this.type = type;
    }

    public DataSourceEventArgs(DataChangedType type, int index) {
        this.type = type;
        this.index = index;
    }

    public DataChangedType getType() {
        return type;
    }

    public void setType(DataChangedType type) {
        this.type = type;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
