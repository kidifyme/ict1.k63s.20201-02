package com.ebr.client.components.base.view;

import com.ebr.client.components.base.event.eventargs.DataEditedEventArgs;

import javax.swing.*;
import java.awt.*;

public abstract class EditDialog<TData>
        extends EventSourceDialog<DataEditedEventArgs<TData>> {
    protected TData t;
    protected GridBagLayout layout;
    protected GridBagConstraints c = new GridBagConstraints();

    public EditDialog(TData t) {
        super((Frame) null, "Edit", true);

        this.t = t;

        setContentPane(new JPanel());
        layout = new GridBagLayout();
        getContentPane().setLayout(layout);
    }

    public void showDialog() {
        this.buildControls();
        this.buildSaveButton();
        this.pack();
        this.setVisible(true);
    }

    protected abstract void buildControls();

    protected void buildSaveButton() {
        JButton saveButton = new JButton("Save");
        saveButton.addActionListener(e -> emitEvent(this, new DataEditedEventArgs<>(getNewData())));
        saveButton.addActionListener(actionEvent -> {
            EditDialog.this.dispose();
        });

        c.gridx = 1;
        c.gridy = getLastRowIndex();
        getContentPane().add(saveButton, c);
    }

    protected abstract TData getNewData();

    protected int getLastRowIndex() {
        layout.layoutContainer(getContentPane());
        int[][] dim = layout.getLayoutDimensions();
        int rows = dim[1].length;
        return rows;
    }
}
