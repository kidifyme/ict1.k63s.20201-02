package com.ebr.client.components.bike.view;

import com.ebr.client.bean.bike.Bike;
import com.ebr.client.bean.bike.EBike;

import javax.swing.*;
import java.awt.*;

public class EBikeEditDialog extends BikeEditDialog {
    private JTextField batteryField;
    private JTextField cyclesField;
    private JTextField remainingField;

    private JTextField errorBatteryField;
    private JTextField errorLoadCyclesField;
    private JTextField errorTimeRemainingField;

    public EBikeEditDialog(Bike editBike) {
        super(editBike);
    }

    @Override
    protected void buildControls() {
        super.buildControls();
        int row = getLastRowIndex();
        EBike ebike = new EBike();
        if (t instanceof EBike) {
            ebike = (EBike) t;
        }
        JLabel batteryLabel = new JLabel("Battery: ");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(batteryLabel, c);
        batteryField = new JTextField(15);
        batteryField.setText(String.valueOf(ebike.getBatteryPercentage()));
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(batteryField, c);
        errorBatteryField = new JTextField("", 10);
        errorBatteryField.setForeground(Color.RED);
        errorBatteryField.setEditable(false);
        c.gridx = 4;
        c.gridy = row;
        getContentPane().add(errorBatteryField, c);
        validateInt(batteryField, errorBatteryField);

        row = getLastRowIndex();
        JLabel cyclesLabel = new JLabel("Cycles: ");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(cyclesLabel, c);
        cyclesField = new JTextField(15);
        cyclesField.setText(String.valueOf(ebike.getLoadCycles()));
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(cyclesField, c);
        errorLoadCyclesField = new JTextField("", 10);
        errorLoadCyclesField.setForeground(Color.RED);
        errorLoadCyclesField.setEditable(false);
        c.gridx = 4;
        c.gridy = row;
        getContentPane().add(errorLoadCyclesField, c);
        validateInt(cyclesField, errorLoadCyclesField);

        row = getLastRowIndex();
        JLabel remainLabel = new JLabel("Time remaining: ");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(remainLabel, c);
        remainingField = new JTextField(15);
        remainingField.setText(String.valueOf(ebike.getTimeRemaining()));
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(remainingField, c);
        errorTimeRemainingField = new JTextField("", 10);
        errorTimeRemainingField.setForeground(Color.RED);
        errorTimeRemainingField.setEditable(false);
        c.gridx = 4;
        c.gridy = row;
        getContentPane().add(errorTimeRemainingField, c);
        validateDouble(remainingField, errorTimeRemainingField);
    }

    @Override
    protected Bike getNewData() {

        super.getNewData();
        EBike ebike = new EBike();
        if (t instanceof EBike) {
            ebike = (EBike) t;
        }
        ebike.setBatteryPercentage(Integer.parseInt(batteryField.getText()));
        ebike.setLoadCycles(Integer.parseInt(cyclesField.getText()));
        ebike.setTimeRemaining(Double.parseDouble(remainingField.getText()));
        return ebike;
    }
}
