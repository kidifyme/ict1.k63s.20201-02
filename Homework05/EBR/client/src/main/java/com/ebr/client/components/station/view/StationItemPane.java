package com.ebr.client.components.station.view;


import com.ebr.client.bean.station.Station;
import com.ebr.client.components.base.view.ItemPane;

import javax.swing.*;

public class StationItemPane extends ItemPane<Station> {
    private JLabel stationId;
    private JLabel stationName;
    private JLabel stationAddress;
    private JLabel numOfSingleBikes;
    private JLabel numOfTwinBikes;
    private JLabel numOfEBikes;
    private JLabel numOfEmptyDocks;

    public StationItemPane() {
        super();
    }

    @Override
    protected void onBuildControls() {
        super.onBuildControls();

        int row = getLastRowIndex();

        // Label station ID;
        reusableConstraints.gridx = 0;
        reusableConstraints.gridy = row;
        stationId = new JLabel();
        add(stationId, reusableConstraints);

        // Label station name;
        row++;
        reusableConstraints.gridx = 0;
        reusableConstraints.gridy = row;
        stationName = new JLabel();
        add(stationName, reusableConstraints);

        // Label address
        row++;
        reusableConstraints.gridx = 0;
        reusableConstraints.gridy = row;
        stationAddress = new JLabel();
        add(stationAddress, reusableConstraints);

        // Label no. of single bikes
        row++;
        reusableConstraints.gridx = 0;
        reusableConstraints.gridy = row;
        numOfSingleBikes = new JLabel();
        add(numOfSingleBikes, reusableConstraints);

        // Label no. of twin bikes
        row++;
        reusableConstraints.gridx = 0;
        reusableConstraints.gridy = row;
        numOfTwinBikes = new JLabel();
        add(numOfTwinBikes, reusableConstraints);

        // Label no. of Ebikes
        row++;
        reusableConstraints.gridx = 0;
        reusableConstraints.gridy = row;
        numOfEBikes = new JLabel();
        add(numOfEBikes, reusableConstraints);

        // Label no. of empty docks
        row++;
        reusableConstraints.gridx = 0;
        reusableConstraints.gridy = row;
        numOfEmptyDocks = new JLabel();
        add(numOfEmptyDocks, reusableConstraints);
    }

    @Override
    protected void displayData() {
        stationId.setText(String.format("ID: %s", data.getId()));
        stationName.setText(String.format("Name: %s", data.getStationName()));
        stationAddress.setText(String.format("Address: %s", data.getStationAddress()));
        numOfSingleBikes.setText(String.format("Number of single bikes: %s", data.getNumOfSingleBikes()));
        numOfTwinBikes.setText(String.format("Number of twin bikes: %s", data.getNumOfTwinBikes()));
        numOfEBikes.setText(String.format("Number of electric bikes: %s", data.getNumOfEBikes()));
        numOfEmptyDocks.setText(String.format("Number of empty docks: %s", data.getNumOfEmptyDocks()));
    }
}
