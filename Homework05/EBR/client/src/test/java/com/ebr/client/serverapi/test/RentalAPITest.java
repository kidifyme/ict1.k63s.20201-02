package com.ebr.client.serverapi.test;

import com.ebr.client.serverapi.ServerAPI;
import com.ebr.client.serverapi.model.RentalRentRequest;
import com.ebr.client.serverapi.model.RentalReturnRequest;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class RentalAPITest {
    private ServerAPI api = new ServerAPI();
    Map<String, String> queryParams = new HashMap<String, String>();

    @Test
    public void testFailAddRental() {
        RentalRentRequest rentalRentRequest = new RentalRentRequest(false,"bike10","987654321");
        api.addRental(rentalRentRequest);
    }

    @Test
    public void testSuccessAddRental() {
        RentalRentRequest rentalRentRequest = new RentalRentRequest(false,"bike1","987654321");
        System.out.println(api.addRental(rentalRentRequest).toString());
    }

    @Test
    public void testFailReturnBike() {
        RentalReturnRequest rentalReturnRequest = new RentalReturnRequest(false, "sa","987654321");
        api.returnBike(rentalReturnRequest);
    }

}
