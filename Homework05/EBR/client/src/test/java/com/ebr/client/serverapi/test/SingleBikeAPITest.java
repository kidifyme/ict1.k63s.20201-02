package com.ebr.client.serverapi.test;

import com.ebr.client.bean.bike.Bike;
import com.ebr.client.serverapi.ServerAPI;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SingleBikeAPITest {
    private ServerAPI api = new ServerAPI();

    Map<String, String> queryParams = new HashMap<String, String>();

    @Test
    public void testGetAllBikes() {
        queryParams.put("kind", "singlebike,twinbike,ebike");
        ArrayList<Bike> list = api.getBikes(queryParams);
        System.out.println(list);
        assertEquals("Error in getBikes API!", list.size(), 9);
    }

    @Test(timeout = 1000)
    public void testResponse() {
        api.getBikes(null);
    }

    @Test
    public void testUpdateBike() {
        queryParams.put("kind", "singlebike,twinbike,ebike");
        ArrayList<Bike> list = api.getBikes(queryParams);
        assertTrue("No data", list.size() > 0);


        Bike bike = list.get(0);
        String newManufacturer = "New Phoenix";
        bike.setManufacturer(newManufacturer);
        api.updateBike(bike);

    }

    @Test
    public void testAddBike() {
        Bike bike = new Bike("bike10", 10.5, "eco_bike10", new Date(), "BionX", 5.6, "sd", "bike10");
        api.addBike(bike);
    }

    @Test
    public void testDeleteBike() {
        queryParams.put("kind", "singlebike,twinbike,ebike");
        ArrayList<Bike> list = api.getBikes(queryParams);
        api.deleteBike(list.get(0));
    }
}
