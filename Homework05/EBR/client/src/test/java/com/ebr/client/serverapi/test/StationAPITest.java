package com.ebr.client.serverapi.test;

import com.ebr.client.bean.station.Station;
import com.ebr.client.serverapi.ServerAPI;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class StationAPITest {
    private ServerAPI api = new ServerAPI();
    Map<String, String> queryParams = new HashMap<String, String>();

    @Test
    public void testGetAllStations() {
        ArrayList<Station> list = api.getStations(null);
        assertEquals("Error in getBikes API!", list.size(), 4);
    }

    @Test(timeout = 1000)
    public void testResponse() {
        api.getStations(null);
    }

    @Test
    public void testUpdateStation() {
        ArrayList<Station> list = api.getStations(null);
        assertTrue("No data", list.size() > 0);

        Station station = list.get(0);
        String newAddress = "New Address";
        station.setStationAddress(newAddress);
        api.updateStation(station);
    }

    @Test
    public void testAddStation() {
        Station station = new Station("sd", "StationD", "10 Dai Co Viet", 1, 1, 1, 5);
        api.addStation(station);
    }

    @Test
    public void testDeleteStation() {
        ArrayList<Station> list = api.getStations(null);
        api.deleteStation(list.get(0));
    }
}
