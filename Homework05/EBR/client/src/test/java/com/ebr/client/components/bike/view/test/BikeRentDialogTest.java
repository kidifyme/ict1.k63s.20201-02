package com.ebr.client.components.bike.view.test;

import com.ebr.client.components.bike.view.BikeRentDialog;
import com.ebr.client.serverapi.model.PostRentalRentResponse;
import org.junit.Test;

import javax.swing.*;
import java.awt.*;

import static org.junit.Assert.*;

public class BikeRentDialogTest {
    @Test
    public void testBikeRentDialogValidation_Error() {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                PostRentalRentResponse mockedResponse = new PostRentalRentResponse();
                mockedResponse.setStatus("error");

                // Create dialog
                BikeRentDialog dialog = new BikeRentDialog(null, mockedResponse);

                // Find confirm button
                JButton confirmButton = findConfirmButton(dialog);
                assertNotNull(confirmButton);
                assertEquals(false, confirmButton.isEnabled());
            }
        });
    }

    @Test
    public void testBikeRentDialogValidation_Ok() {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                PostRentalRentResponse mockedResponse = new PostRentalRentResponse();
                mockedResponse.setStatus("ok");

                // Create dialog
                BikeRentDialog dialog = new BikeRentDialog(null, mockedResponse);

                // Find confirm button
                JButton confirmButton = findConfirmButton(dialog);
                assertNotNull(confirmButton);
                assertEquals(true, confirmButton.isEnabled());
            }
        });
    }

    private JButton findConfirmButton(JDialog dialog) {
        Component[] components = dialog.getContentPane().getComponents();
        for (Component component : components) {
            if (component instanceof JButton && ((JButton) component).getText().equals("Confirm")) {
                return (JButton) component;
            }
        }

        return null;
    }
}
